﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomatizacionEtiquetado.Comun.Clases
{
    /// <summary>
    /// Esta fue rafa y rodo
    /// </summary>
    public class Etiqueta
    {

        public Etiqueta(string xTexto, bool xMostrar, int xX, int xY, int xWidth, int xHeight, string xValor)
        {
            this.mTexto = xTexto;
            this.mMostrar = xMostrar;
            this.mX = xX;
            this.mY = xY;
            this.mWidth = xWidth;
            this.mHeight = xHeight;
            this.mValor = xValor;
        }

        private string mTexto;
        public string Texto
        {
            get { return mTexto; }
        }

        private bool mMostrar;
        public bool Mostrar
        {
            get { return mMostrar; }
        }

        private int mX;
        public int X
        {
            get { return mX; }
        }

        private int mY;
        public int Y
        {
            get { return mY; }
        }

        private int mWidth;
        public int Width
        {

            get { return mWidth; }
        }

        private int mHeight;
        public int Height
        {
            get { return mHeight; }
        }

        private string mValor;
        public string Valor { get { return mValor; } }

    }
}
