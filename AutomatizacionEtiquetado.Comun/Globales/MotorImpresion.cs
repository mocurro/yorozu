﻿using System;
using System.Collections.Generic;
using System.Text;
using Intermec.Printer;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using AutomatizacionEtiquetado.Comun.Globales;
using Automatizacion_de_etiquetado.Enumeradores;

namespace AutomatizacionEtiquetado.UI.Base
{
    public static class MotorImpresion
    {
        /// <summary>
        /// Delegado para cuando existe un error de impresión
        /// </summary>
        /// <param name="mensajeError">El mensaje de error</param>
        /// <param name="plantilla">La plantilla que se trató de imprimir</param>
        public delegate void MotorImpresion_Error(string mensajeError,Drawing plantilla);

        /// <summary>
        /// Delegado para cuando existe una impresión exitosa
        /// </summary>
        /// <param name="incrementaConteo">Deterimina si la impresión incrementa contador</param>
        public delegate void MotorImpresion_Exitosa(bool incrementaConteo);

        /// <summary>
        /// Evento que ocurre cuando existe un error en la impresión
        /// </summary>
        public static event MotorImpresion_Error ErrorImpresion;

        /// <summary>
        /// Evento que ocurre cuando la impresión fue exitpsa
        /// </summary>
        public static event MotorImpresion_Exitosa ImpresionExitosa;

        /// <summary>
        /// Control de impresión
        /// </summary>
        static PrintControl printControl;

        static Drawing drawing2 = new Drawing();

        static string[] xmlPlantilla2;


        /// <summary>
        /// Envía datos a la impresora
        /// </summary>
        /// <param name="plantilla">La plantilla a enviar</param>
        public static void Imprimir(Drawing plantilla,bool incrementarConteo)
        {
            Imprimir(plantilla, 1,incrementarConteo);
        }

        /// <summary>
        /// Envía datos a la impresora
        /// </summary>
        /// <param name="plantilla">La plantilla a enviar</param>
        public static void Imprimir(Drawing plantilla, bool incrementarConteo, int Copias)
        {
            Imprimir(plantilla, Copias, incrementarConteo);
        }

        /// <summary>
        /// Envía datos a la impresora
        /// </summary>
        /// <param name="plantilla">La plantilla a enviar</param>
        /// <param name="numeroDeCopias">Número de veces que la etiqueta será impresa</param>
        public static void Imprimir(Drawing plantilla, int numeroDeCopias,bool incrementarConteo)
        {
            if (printControl == null)
            {
                printControl = new PrintControl();
            }
                State state = printControl.PrintFeed(plantilla, numeroDeCopias);
                switch (state)
                {
                    case State.NoError:
                        //ImpresionExitosa(incrementarConteo);
                        break;
                    case State.OutOfMedia:
                        ErrorImpresion("La impresora no tiene papel",plantilla);
                        break;
                    case State.OutOfRibbon:
                        ErrorImpresion("La impresora no tiene ribbón",plantilla);
                        break;
                    case State.PrintheadLifted:
                        ErrorImpresion("El cabezal está abierto",plantilla);
                        break;
                    case State.GeneralError:
                        ErrorImpresion("Error general de impresión",plantilla);
                        break;
                    default:
                        ErrorImpresion(string.Format("Hubo un error en la impresión: {0}", state.ToString()),plantilla);
                        break;
                }
                printControl.Dispose();
                printControl = null;
        }

        /// <summary>
        /// Realiza la impresión de una etiqueta
        /// </summary>
        /// <param name="rutaPlantilla">La ruta donde se encuentra almacenada la plantilla</param>
        /// <param name="variables">Las variables a reemplazar</param>
        /// <param name="incrementarConteo">Contiene si incrementa el conteo de piezas impresas</param>
        public static void Imprimir(string rutaPlantilla,Dictionary<string,string> variables, bool incrementarConteo)
        {
            Globales.RegistrarEvento("Imprimiendo", TiposMensajes.Aviso);
            Drawing drawing = new Drawing();
            XmlSerializer xmlDeserializer = new XmlSerializer(typeof(List<Drawing.Base>));
            TextReader textReader;
            string plantillaFinal = "/home/user/apps/PlantillaFinal.XML";

           

            string[] xmlPlantilla = File.ReadAllLines(rutaPlantilla, Encoding.UTF8);
            string plantilla = string.Empty;

            foreach (string texto in xmlPlantilla)
                plantilla += texto;

            //Reemplaza los valores en el xml
            foreach (var variable in variables)
                plantilla = plantilla.Replace(variable.Key, variable.Value);

            //Elimina el archivo si existe
            if (File.Exists(plantillaFinal)) File.Delete(plantillaFinal);

            //Guarda el archivo de plantilla final
            using (FileStream fileStream = File.Create(plantillaFinal))
            {
                Byte[] texto = new UTF8Encoding(true).GetBytes(plantilla);
                fileStream.Write(texto, 0, texto.Length);
            }

            textReader = new StreamReader(plantillaFinal);
            drawing.DrawingObjects = (List<Drawing.Base>)xmlDeserializer.Deserialize(textReader);


            //textReader = new StreamReader(plantillaEjemplo);
            //drawing.DrawingObjects = (List<Drawing.Base>)xmlDeserializer.Deserialize(textReader);
            

            //setDataField(drawing,"Variable","Dato");
            // esto podría quitar procesamiento al crear "Plantilla Final" 

            textReader.Close();
            //ErrorImpresion("La impresora no tiene papel", plantilla);
            Imprimir(drawing,incrementarConteo);
            drawing.Dispose();

        }

        /// <summary>
        /// Realiza la impresión de una etiqueta
        /// </summary>
        /// <param name="rutaPlantilla">La ruta donde se encuentra almacenada la plantilla</param>
        /// <param name="variables">Las variables a reemplazar</param>
        /// <param name="incrementarConteo">Contiene si incrementa el conteo de piezas impresas</param>
        public static int Imprimir(string rutaPlantilla, Dictionary<string, string> variables, bool incrementarConteo,int Cantidad,String [] imagenes)
        {
            int n = 0;
            try
            {
                
                Globales.RegistrarEvento("Imprimiendo", TiposMensajes.Aviso);
                Drawing drawing = new Drawing();


               
                XmlSerializer xmlDeserializer = new XmlSerializer(typeof(List<Drawing.Base>));
                TextReader textReader;
                string plantillaFinal = "/home/user/apps/PlantillaFinal.XML";

                string[] xmlPlantilla = File.ReadAllLines(rutaPlantilla, Encoding.UTF8);
                string plantilla = string.Empty;
                

                //if (xmlPlantilla2 == null) {
                //    xmlPlantilla2 = File.ReadAllLines(rutaPlantilla, Encoding.UTF8);
                //}

                //string[] xmlPlantilla = File.ReadAllLines(rutaPlantilla, Encoding.UTF8);
                //string plantilla = string.Empty;

                foreach (string texto in xmlPlantilla)
                    plantilla += texto;

                //Reemplaza los valores en el xml
                foreach (var variable in variables)
                    plantilla = plantilla.Replace(variable.Key, variable.Value);

                //Elimina el archivo si existe
                if (File.Exists(plantillaFinal)) File.Delete(plantillaFinal);

                //Guarda el archivo de plantilla final
                using (FileStream fileStream = File.Create(plantillaFinal))
                {
                    Byte[] texto = new UTF8Encoding(true).GetBytes(plantilla);
                    fileStream.Write(texto, 0, texto.Length);
                }


                try {

                    textReader = new StreamReader(plantillaFinal);

                    drawing.DrawingObjects = (List<Drawing.Base>)xmlDeserializer.Deserialize(textReader);

                    //Drawing.Barcode bar = new Drawing.Barcode(50, 50, "DATAMATRIX");
                    //bar.Data = "1234";
                    //bar.WidthEnlargement = 10;
                    //drawing += bar;


                    textReader.Close();
                }
                catch (Exception ex) {
                    ErrorImpresion(string .Format ("{0};{1}",ex.Message ,ex.StackTrace ), drawing);
                }
               
                // agrega el logo de verifID

                //Drawing.Image objImagen = new Drawing.Image(540, 10, "/home/user/images/Logo.png");
                //drawing += objImagen;

                //setDataField(drawing,"Variable","Dato");
                // esto podría quitar procesamiento al crear "Plantilla Final" 



                //if (imagenes != null)
                //{
                //    foreach (string imagen in imagenes)
                //    {
                //        String[] datosImagen = imagen.Split(',');
                //        int x = int.Parse(datosImagen[0]);
                //        int y = int.Parse(datosImagen[1]);
                //        string ruta = datosImagen[2];
                //        Drawing.Image objImagen = new Drawing.Image(x, y, ruta);
                //        drawing += objImagen;
                //    }
                //}


                    Imprimir(drawing, incrementarConteo, 1);
                    drawing.Dispose();
                   
               

                return n;

            }
            catch (Exception) {
                return n;
            }
            





            

        }

        /// <summary>
        /// Realiza la impresión de una etiqueta
        /// </summary>
        /// <param name="rutaPlantilla">La ruta donde se encuentra almacenada la plantilla</param>
        /// <param name="variables">Las variables a reemplazar</param>
        /// <param name="incrementarConteo">Contiene si incrementa el conteo de piezas impresas</param>
        public static int Imprimir2(string rutaPlantilla, Dictionary<string, string> variables, bool incrementarConteo, int Cantidad)
        {
            int n = 0;
            try
            {

                Drawing drawing = new Drawing();

                XmlSerializer xmlDeserializer = new XmlSerializer(typeof(List<Drawing.Base>));
                TextReader textReader;
                string plantillaFinal = "/home/user/apps/PlantillaFinal.XML";



                if (xmlPlantilla2 == null)
                {
                    xmlPlantilla2 = File.ReadAllLines(rutaPlantilla, Encoding.UTF8);
                }

                //string[] xmlPlantilla = File.ReadAllLines(rutaPlantilla, Encoding.UTF8);
                string plantilla = string.Empty;

                foreach (string texto in xmlPlantilla2)
                    plantilla += texto;

                //Reemplaza los valores en el xml
                foreach (var variable in variables)
                    plantilla = plantilla.Replace(variable.Key, variable.Value);

                //Elimina el archivo si existe
                if (File.Exists(plantillaFinal)) File.Delete(plantillaFinal);

                //Guarda el archivo de plantilla final
                using (FileStream fileStream = File.Create(plantillaFinal))
                {
                    Byte[] texto = new UTF8Encoding(true).GetBytes(plantilla);
                    fileStream.Write(texto, 0, texto.Length);
                }
                try
                {

                    textReader = new StreamReader(plantillaFinal);

                    drawing.DrawingObjects = (List<Drawing.Base>)xmlDeserializer.Deserialize(textReader);


                    textReader.Close();
                }
                catch (Exception ex)
                {
                    ErrorImpresion(string.Format("{0};{1}", ex.Message, ex.StackTrace), drawing);
                }




                Imprimir(drawing, incrementarConteo, 1);
                drawing.Dispose();
                //drawing2 = null;

                try
                {
                    File.Delete(plantillaFinal);
                }
                catch (Exception) { 
                }

                return n;

            }
            catch (Exception)
            {
                return n;
            }
            
        }

        /// <summary>
        /// Realiza la impresión de una etiqueta
        /// </summary>
        /// <param name="rutaPlantilla">La ruta donde se encuentra almacenada la plantilla</param>
        /// <param name="variables">Las variables a reemplazar</param>
        /// <param name="incrementarConteo">Contiene si incrementa el conteo de piezas impresas</param>
        public static int Imprimir3(string rutaPlantilla, Dictionary<string, string> variables, bool incrementarConteo, int Cantidad,bool usarAnterior)
        {
            int n = 0;
            try
            {
                string plantillaFinal = "/home/user/apps/PlantillaFinal.XML";
                string plantilla = string.Empty;
                TextReader textReader;
                
                XmlSerializer xmlDeserializer = new XmlSerializer(typeof(List<Drawing.Base>));
                if (usarAnterior)
                {
                    

                        Imprimir(drawing2, incrementarConteo, 1);

                        return n;
                }
                else {
                    drawing2 = new Drawing();

                    if (xmlPlantilla2 == null)
                    {
                        xmlPlantilla2 = File.ReadAllLines(rutaPlantilla, Encoding.UTF8);
                    }

                    //string[] xmlPlantilla = File.ReadAllLines(rutaPlantilla, Encoding.UTF8);
                   

                    foreach (string texto in xmlPlantilla2)
                        plantilla += texto;

                    //Reemplaza los valores en el xml
                    foreach (var variable in variables)
                        plantilla = plantilla.Replace(variable.Key, variable.Value);

                    //Elimina el archivo si existe
                    if (File.Exists(plantillaFinal)) File.Delete(plantillaFinal);

                    //Guarda el archivo de plantilla final
                    using (FileStream fileStream = File.Create(plantillaFinal))
                    {
                        Byte[] texto = new UTF8Encoding(true).GetBytes(plantilla);
                        fileStream.Write(texto, 0, texto.Length);
                    }
                    try
                    {

                        textReader = new StreamReader(plantillaFinal);

                        drawing2.DrawingObjects = (List<Drawing.Base>)xmlDeserializer.Deserialize(textReader);


                        textReader.Close();
                    }
                    catch (Exception ex)
                    {
                        ErrorImpresion(string.Format("{0};{1}", ex.Message, ex.StackTrace), drawing2);
                    }

                    Imprimir(drawing2, incrementarConteo, 1);
                    //drawing2.Dispose();

                    return n;
                }
                

            }
            catch (Exception)
            {
                return n;
            }

        }

        /// <summary>
        /// Realiza la impresión de una etiqueta
        /// </summary>
        /// <param name="rutaPlantilla">La ruta donde se encuentra almacenada la plantilla</param>
        /// <param name="variables">Las variables a reemplazar</param>
        /// <param name="incrementarConteo">Contiene si incrementa el conteo de piezas impresas</param>
        public static int Imprimir4(string rutaPlantilla, Dictionary<string, string> variables)
        {
            int n = 0;
            try
            {
                string plantillaFinal = "/home/user/apps/PlantillaFinal.XML";
                string plantilla = string.Empty;
                TextReader textReader;
                XmlSerializer xmlDeserializer = new XmlSerializer(typeof(List<Drawing.Base>));
               
                    drawing2 = new Drawing();

                    if (xmlPlantilla2 == null)
                    {
                        xmlPlantilla2 = File.ReadAllLines(rutaPlantilla, Encoding.UTF8);
                    }

                    //string[] xmlPlantilla = File.ReadAllLines(rutaPlantilla, Encoding.UTF8);


                    foreach (string texto in xmlPlantilla2)
                        plantilla += texto;

                    //Reemplaza los valores en el xml
                    foreach (var variable in variables)
                        plantilla = plantilla.Replace(variable.Key, variable.Value);

                    //Elimina el archivo si existe
                    if (File.Exists(plantillaFinal)) File.Delete(plantillaFinal);

                    //Guarda el archivo de plantilla final
                    using (FileStream fileStream = File.Create(plantillaFinal))
                    {
                        Byte[] texto = new UTF8Encoding(true).GetBytes(plantilla);
                        fileStream.Write(texto, 0, texto.Length);
                    }
                    try
                    {

                        textReader = new StreamReader(plantillaFinal);

                        drawing2.DrawingObjects = (List<Drawing.Base>)xmlDeserializer.Deserialize(textReader);


                        textReader.Close();
                    }
                    catch (Exception ex)
                    {
                        ErrorImpresion(string.Format("{0};{1}", ex.Message, ex.StackTrace), drawing2);
                    }

                    return n;


            }
            catch (Exception)
            {
                return n;
            }

        }


        public static void TestFeeed()
        {
            if (printControl == null)
                printControl = new PrintControl();
                        
            printControl.TestFeed();

        }


        public static void ImprimirPrueba()
        {
            Drawing resultado = new Drawing(400, 115);
            Drawing.Text imagenPrueba = new Drawing.Text(15, 40, "Univers Bold","Impresión de prueba");
            if (printControl == null)
                printControl = new PrintControl();

            resultado += imagenPrueba;
            printControl.PrintFeed(resultado, 1);
            printControl.Dispose();

        }
    }
}
