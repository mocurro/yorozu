﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Automatizacion_de_etiquetado.Enumeradores;
using Intermec.Printer;
using System.IO;

namespace AutomatizacionEtiquetado.Comun.Globales
{
    /// <summary>
    /// Clase encargada de la comunicación serial con el puerto
    /// </summary>
    public static class MotorSerial
    {

        /// <summary>
        /// Delegado para cuando el motor arranca
        /// </summary>
        public delegate void MotorSerial_Iniciado();

        /// <summary>
        /// Delegado para cuando el motor se deteine
        /// </summary>
        public delegate void MotorSerial_Detenido();

        /// <summary>
        /// Delegado para cuando se recibe información del puerto
        /// </summary>
        /// <param name="mensaje">El texto recibido del puerto</param>
        public delegate void MotorSerial_DatosRecibidos(string mensaje);

        /// <summary>
        /// Delegado para cuando se recibe un error de comunicación con el puerto
        /// </summary>
        /// <param name="mensaje">El mensaje de error</param>
        public delegate void MotorSerial_ErrorInesperado(string mensaje);

        /// <summary>
        /// Evento que se dispara cuando el motor arranca
        /// </summary>
        public static event MotorSerial_Iniciado MotorInicia;

        /// <summary>
        /// Evento que se dispara cuando el motor se detiene
        /// </summary>
        public static event MotorSerial_Detenido MotorDetiene;

        /// <summary>
        /// Evento que se dispara cuando se reciben datos del puerto
        /// </summary>
        public static event MotorSerial_DatosRecibidos MotorRecibeDatos;

        /// <summary>
        /// Evento que se dispara cuando se recibie un error inesperado
        /// </summary>
        public static event MotorSerial_ErrorInesperado MotorErrorInesperado;
    
        /// <summary>
        /// Contiene el objeto para representar el puerto
        /// </summary>
        private static Intermec.Printer.Communication.SerialPort puertoCom;// = new Communication.SerialPort("/dev/ttyS0");

        /// <summary>
        /// Contiene el hilo que monitorea el puerto en busca de información
        /// </summary>
        private static Thread hiloMotor = new Thread(new ParameterizedThreadStart(MonitorearPuerto));

        /// <summary>
        /// Determina si se sigue monitoreando el puerto
        /// </summary>
        private static Boolean continuarMonitoreo = false;

        static bool cadenaIncompleta = true;

        static string cadenaParcial = string.Empty;
        
        /// <summary>
        /// Arranca el proceso de monitoreo
        /// </summary>
        public static void Iniciar()
        {
            InicializarPuerto();
        }

        /// <summary>
        /// Inicializa el puerto de comunicación
        /// </summary>
        private static void InicializarPuerto()
        {
            try
            {
                Globales.RegistrarEvento("Iniciando monitoreo de puerto", TiposMensajes.Aviso);
                puertoCom = new Intermec.Printer.Communication.SerialPort("/dev/ttyS0");             
                puertoCom.BaudRate = 9600;
                puertoCom.Parity = System.IO.Ports.Parity.None;
                puertoCom.StopBits = System.IO.Ports.StopBits.One;
                puertoCom.DataBits = 8;
                puertoCom.Handshake = System.IO.Ports.Handshake.None;
                //puertoCom.ReadBufferSize = 2000000;
                //puertoCom.WriteBufferSize = 2000000;
                
                if (puertoCom.IsOpen)
                    puertoCom.Close();
                puertoCom.Open();

                //byte[] cadena = File.ReadAllBytes("/home/user/apps/Cadena.txt");
                //string resultado = string.Empty;
                //foreach (byte valor in cadena)
                //{
                //    if (valor == 0)
                //    {
                //    }
                //    else
                //    {
                //        resultado += (char)valor;
                //    }
                //}

                continuarMonitoreo = true;
                hiloMotor.Start(puertoCom);
            }
            catch (Exception ex)
            {
                string mensajeError = string.Format("Hubo un error en el arranque del puerto {0}", ex.Message);
                if (ex.InnerException != null)
                    mensajeError += string.Format(" - {0}", ex.InnerException.Message);
                MotorErrorInesperado(mensajeError);
            }

        }

        /// <summary>
        /// Realiza el monitoreo del puerto
        /// </summary>
        /// <param name="data">El objeto que representa el puerto comm</param>
        private static void MonitorearPuerto(object data)
        {
            Intermec.Printer.Communication.SerialPort puertoSerie = (Intermec.Printer.Communication.SerialPort)data;
            while (continuarMonitoreo)
            {
                if (puertoSerie.IsOpen)
                {
                    string datosRecibidos = string.Empty;
                    if (puertoSerie.BytesToRead > 0)
                    {
                        byte[] buffer = new byte[600];
                        puertoSerie.Read(buffer, 0, buffer.Length);

                        foreach (byte valor in buffer)
                        {
                            if (valor != 0)
                                datosRecibidos += (char)valor;
                            else
                                datosRecibidos += " ";
                        }
                        datosRecibidos = datosRecibidos.TrimEnd();

                        if (!datosRecibidos.EndsWith("<FF><ETX>"))
                        {

                            cadenaIncompleta = true;
                            cadenaParcial += datosRecibidos;
                            Globales.RegistrarEvento("Cadena incompleta", TiposMensajes.Advertencia);
                        }
                        else
                        {
                            cadenaIncompleta = false;
                            cadenaParcial += datosRecibidos;
                            Globales.RegistrarEvento("Cadena completa", TiposMensajes.Advertencia);
                        }
                        if (!cadenaIncompleta)
                        {
                            MotorRecibeDatos(cadenaParcial);
                            cadenaParcial = string.Empty;
                        }
                    }
                    else
                    {
                        Globales.RegistrarEvento("Sin datos del puerto", TiposMensajes.Aviso);
                    }
                }
                Thread.Sleep(1000);
            }
            if (puertoCom != null)
            {
                puertoCom.Close();
                puertoCom.Dispose();
            }
            
            puertoCom = null;
            Globales.RegistrarEvento("Puerto detenido", TiposMensajes.Aviso);
        }

        /// <summary>
        /// Detiene el monitoreo del puerto
        /// </summary>
        public static void Detener()
        {
            if (hiloMotor == null) return;
            if (hiloMotor.IsAlive)
            {
                continuarMonitoreo = false;
                hiloMotor = null;
            }
        }


        public static void Reiniciar()
        {
            continuarMonitoreo = true;
            
            hiloMotor = new Thread(new ParameterizedThreadStart(MonitorearPuerto));
            InicializarPuerto();
        }
    }
}
