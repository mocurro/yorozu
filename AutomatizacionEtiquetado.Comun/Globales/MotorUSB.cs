﻿using System;
using System.Collections.Generic;
using System.Text;
using Automatizacion_de_etiquetado.Enumeradores;
using Intermec.Printer;
using System.IO;
using System.Threading;

namespace AutomatizacionEtiquetado.Comun.Globales
{
    /// <summary>
    /// Clase para representar los datos obtenidos del puerto USB
    /// </summary>
    public static class MotorUSB
    {
        /// <summary>
        /// Delegado que se invoca cuando existe un error inesperado en la comunicación
        /// </summary>
        /// <param name="mensaje">La descripción del error</param>
        public delegate void MotorUSB_ErrorInesperado(string mensaje);

        /// <summary>
        /// Delegado que se invoca cuando no hay puertos usb disponibles
        /// </summary>
        public delegate void MotorUSB_SinPuertosDisponibles();

        /// <summary>
        /// Delegado que se invoca cuando el único puerto disponible no es una entrada de texto
        /// </summary>
        /// <param name="mensaje">El mensaje a mostrar</param>
        public delegate void MotorUSB_TipoDispositivoInvalido(string mensaje);

        /// <summary>
        /// Delegado que se invoca cuando se reciben datos del lector
        /// </summary>
        /// <param name="datos">Los datos recibidos</param>
        public delegate void MotorUSB_DatosRecibidos(string datos);

        /// <summary>
        /// Evento que se ejecuta cuando existe un error inesperado en la lectura
        /// </summary>
        public static event MotorUSB_ErrorInesperado ErrorInesperado;

        /// <summary>
        /// Evento que se ejecuta cuando no hay dispositivos conectados
        /// </summary>
        public static event MotorUSB_SinPuertosDisponibles SinDispositivosEncontrados;

        /// <summary>
        /// Evento que se ejecuta cuando el dispositivo conectado no es una entrada de texto
        /// </summary>
        public static event MotorUSB_TipoDispositivoInvalido DispositivoInvalido;

        /// <summary>
        /// Evento que se ejecuta cuando se reciben datos del lector
        /// </summary>
        public static event MotorUSB_DatosRecibidos DatosRecibidos;
        
        /// <summary>
        /// Contiene la lista de puertos
        /// </summary>
        private static string[] puertos;

        static internal Communication.USBHost kbdHost;
        static internal Communication.USBHost scanHost;
        static internal Intermec.Printer.UI.Canvas.Timer tmr = new Intermec.Printer.UI.Canvas.Timer();

        static bool kbdFound = false;
        static bool scanFound = false;

        const char StartSentinel = '[';
        const char EndSentinel = ']';

        //static string cadenaCompleta = string.Empty;
        //static bool seRecibeCompleta = false;

        /// <summary>
        /// Comienza con el proceso de escaneo
        /// </summary>
        public static void IniciarEscaneo()
        {
            Globales.RegistrarEvento("Buscando puertos USB", TiposMensajes.Aviso);
            puertos = Communication.USBHost.GetPortNames();

            if (puertos.Length == 0)
            {
                Globales.RegistrarEvento("No hay puertos disponibles", TiposMensajes.Advertencia);
                SinDispositivosEncontrados();
                return;
            }
            else
            {
                foreach (string nombrePuerto in puertos)
                {
                    Communication.USBHost usbHost = new Communication.USBHost(nombrePuerto);
                    usbHost.Open();

                    if (usbHost.IsOpen)
                    {
                        scanFound = true;
                        scanHost = usbHost;                        
                        usbHost.Close();
                    }
                }
                if (!(kbdFound || scanFound))
                {
                    SinDispositivosEncontrados();
                    return;
                }
                else
                {
                    if (kbdFound)
                        kbdHost.Open();
                    if (scanFound)
                        scanHost.Open();

                    Console.WriteLine("Setting up the timer.");
                    //tmr = new Intermec.Printer.UI.Canvas.Timer();
                    tmr.Interval = 100;
                    tmr.Tick += new Intermec.Printer.UI.Canvas.TimerEventHandler(tmr_Tick);
                    tmr.Start();

                    if (kbdFound)
                    {
                        kbdHost.Close();
                        kbdHost.Dispose();
                    }

                    if (scanFound)
                    {
                        scanHost.Close();
                        scanHost.Dispose();
                    }

                }
            }

        }

        /// <summary>
        /// Detiene la lectura de puertos
        /// </summary>
        public static void DetenerLectura()
        {
            if (kbdFound)
            {
                kbdHost.Close();
                kbdHost.Dispose();
            }

            if (scanFound)
            {
                scanHost.Close();
                scanHost.Dispose();
            }
            tmr.Dispose();
            //tmr.Stop();
            //tmr = null;
            //Globales.RegistrarEvento("Monitoreo USB detenido", TiposMensajes.Aviso);
        }

        /// <summary>
        /// Realiza la lectura de los puertos 
        /// </summary>
        static void tmr_Tick(object sender, Intermec.Printer.UI.Canvas.TimerEventArgs e)
        {
            if (kbdFound)
                EntradaTeclado();
            if (scanFound)
                LecturaDeEscaner();
        }

        /// <summary>
        /// Método que se ejecuta cuando la entrada proviene de un teclado
        /// </summary>
        private static void EntradaTeclado()
        {
            if (kbdHost.IsOpen)
            {
                FileStream fileStream = kbdHost.GetStream();
                try
                {
                    int byteIn;
                    if ((byteIn = fileStream.ReadByte()) >= 0)
                    {
                        if (byteIn == 8)
                        {
                            //Console.WriteLine("Backspace pressed.");
                            //kbdIn.Data = kbdIn.Data.Substring(0, kbdIn.Data.Length - 1);
                            //fmCanvas.Refresh();
                        }
                        else if (byteIn == 27)
                        {
                            //Console.WriteLine("ESC pressed, exiting ...");
                            //tmr.Stop();
                            //fmCanvas.Exit();
                        }
                        else
                        {
                            string data = ((char)byteIn).ToString();
                            DatosRecibidos(data);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ErrorInesperado(string.Format("Hubo un error en la lectura del teclado: {0}", ex.Message));
                }
            }
        }


        static string cadenaParcial = string.Empty;

        static bool cadenaCompleta = false;

        /// <summary>
        /// Método que se ejecuta cuando la entrada proviene de un escáner
        /// </summary>
        private static void LecturaDeEscaner()
        {
            scanHost.Open();
            if (scanHost.IsOpen)
            {
                tmr.Stop();
                FileStream fileStream = scanHost.GetStream();

                try
                {
                    int i;
                    string data = "";
                    Byte[] bytes = new Byte[256];
                    int sIdx;
                    int eIdx;

                    bool keepReading = true;

                    while (keepReading = ((i = fileStream.Read(bytes, 0, bytes.Length)) > 0))
                    {
                        // Start sentinel in stream?
                        sIdx = (bytes[0] == StartSentinel) ? 1 : 0;

                        // End sentinel in stream?
                        keepReading = (bytes[i - 1] != EndSentinel);
                        eIdx = i - sIdx - (keepReading ? 0 : 1);

                        data += System.Text.Encoding.ASCII.GetString(bytes, sIdx, eIdx);
                        //data += fileStream.ReadByte();
                        Thread.Sleep(10);
                    }

                    if (data != string.Empty)
                    {

                        if (!data.EndsWith("\n"))
                        {
                            cadenaCompleta = false;
                            cadenaParcial += data;
                            Globales.RegistrarEvento("Cadena USB incompleta: " + cadenaParcial, TiposMensajes.Aviso);
                        }
                        else
                        {
                            cadenaCompleta = true;
                            cadenaParcial += data.Replace("\n",string.Empty);
                            Globales.RegistrarEvento("Cadena USB Completa: " + cadenaParcial, TiposMensajes.Aviso);
                        }
                        if (cadenaCompleta)
                        {
                            DatosRecibidos(cadenaParcial);
                            cadenaParcial = string.Empty;
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception reading scanner: {0}", ex.Message);
                }
                tmr.Start();
            }
        }
    }
}
