﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace AutomatizacionEtiquetado.Comun.Globales
{
    public static class ManejadorMemoria
    {
        [DllImport("psapi.dll")]
        public static extern bool EmptyWorkingSet(IntPtr hProcess);

        public static void LiberarMemoria()
        {
            Process pProcess = Process.GetCurrentProcess();
            bool bRes = EmptyWorkingSet(pProcess.Handle);
            Globales.RegistrarEvento("Memoria liberada", Automatizacion_de_etiquetado.Enumeradores.TiposMensajes.Aviso);
            if (!bRes)
            {
                Globales.RegistrarEvento("No se libero la memoria", Automatizacion_de_etiquetado.Enumeradores.TiposMensajes.Aviso);
            }
        }

    }
}
