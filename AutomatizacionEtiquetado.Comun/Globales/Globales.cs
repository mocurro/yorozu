﻿using System;
using System.Collections.Generic;
using System.Text;
using Intermec.Printer;
using Automatizacion_de_etiquetado.Enumeradores;
using System.Runtime.InteropServices;
using System.IO;
using AutomatizacionEtiquetado.Comun.Clases;

namespace AutomatizacionEtiquetado.Comun.Globales
{
    /// <summary>
    /// Clase para uso general de la aplicación
    /// </summary>
    public static class Globales
    {
       

        /// <summary>
        /// Devuelve el color general de la apliacción
        /// </summary>
        /// <returns>Devuelve un color</returns>
        public static Color ColorAplicacion()
        {
            return new Color(204, 255, 153);
        }

        /// <summary>
        /// Color para pantallas indicando que hay un error
        /// </summary>
        /// <returns>Devuelve un objeto de tipo color</returns>
        public static Color ColorAplicacionError()
        {
            return new Color(255, 140, 0);
        }

       

        /// <summary>
        /// Escribe en consola el log de la aplicación
        /// </summary>
        /// <param name="evento">El evento a registrar</param>
        /// <param name="tipoMensaje">El tipo de evento</param>
        public static void RegistrarEvento(string evento, TiposMensajes tipoMensaje)
        {
            evento += string.Format(" - {0} {1} {2}", tipoMensaje, DateTime.Now,"\n");
            Console.Write(evento);
        }


        public static Etiqueta CrearEtiqueta(string variable,bool visible, int x, int y, int ancho, int alto,string valor)
        {
            return new Etiqueta(variable, visible, x, y, ancho, alto, valor);
        }

        
        /// <summary>
        /// Obtiene la plantilla de impresión, si no existe crea y devuelve una generica
        /// </summary>
        /// <returns>Plantilla de impresión</returns>
        public static string ObtenerPlantilla()
        {
            string resultado = string.Empty;
            if (!File.Exists(Constantes.Constantes.RutaArchivoPlantilla))
            {
                return CrearPlantilla();
            }
            else
            {
                StreamReader streamReader = new StreamReader(Constantes.Constantes.RutaArchivoPlantilla);
                resultado = File.ReadAllText(Constantes.Constantes.RutaArchivoPlantilla);
            }
            return resultado;
        }

        /// <summary>
        /// Crea una plantila génerica
        /// </summary>
        /// <returns>Plantilla de impresión</returns>
        public static string CrearPlantilla()
        {
            string resultado = string.Empty;
            using (FileStream fileStream = File.Create(Constantes.Constantes.RutaArchivoPlantilla))
            {



                //Byte[] texto = new UTF8Encoding(true).GetBytes(cantidadPallet);
                //fileStream.Write(texto, 0, texto.Length);
            }
            return resultado;
        }
    }
}
