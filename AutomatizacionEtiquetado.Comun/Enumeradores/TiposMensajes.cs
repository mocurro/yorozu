﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Automatizacion_de_etiquetado.Enumeradores
{
    /// <summary>
    /// Contiene los tipos de mensajes que se registran en la aplicación
    /// </summary>
    public enum TiposMensajes 
    {
        Aviso = 0,
        Advertencia = 1,
        ErrorInesperado = 2,
        Pregunta = 3
    }

    public  enum IniciaraPalicacion: int { 
        ConComparacion= 1,
        SinComparacion=2,
        ArchivoVacio=3,
        FormatoInvalido=4
    }
}
