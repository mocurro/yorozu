﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomatizacionEtiquetado.Comun.Constantes
{
    /// <summary>
    /// Clase para contención de constantes
    /// </summary>
    public static class Constantes
    {
        /// <summary>
        /// Constante para identificación de la ruta del 
        /// archivo de usuarios
        /// </summary>
        public const string RutaArchivoUsuarios = "/home/user/apps/Usuarios.txt";

        /// <summary>
        /// Consante para identificación de la ruta del
        /// archivo de números de parte
        /// </summary>
        public const string RutaArchivoNumerosDeParte = "/home/user/apps/NumerosDeParte.txt";

        /// <summary>
        /// Constante para identificación de la ruta del
        /// archivo de configuraciones
        /// </summary>
        public const string RutaArchivoConfiguracion = "/home/user/apps/Configuracion.txt";

        /// <summary>
        /// Constante para identificación de la ruta de la 
        /// plantilla de impresión
        /// </summary>
        public const string RutaArchivoPlantilla = "/home/user/apps/Plantilla.XML";

        /// <summary>
        /// Constante para identificación de la variable
        /// de la configuración para la cantidad máxima de pallet
        /// </summary>
        public const string CantidadPallet = "CantidadPallet";

        /// <summary>
        /// Constante para identificación de la variable
        /// de la configuración para la cantidad máxima de pallet
        /// </summary>
        public const string RutaArchivoTurnos = "/home/user/apps/TURNOS.TXT";

        /// <summary>
        /// Consante para identificación de la ruta del
        /// archivo de cosecutivo
        /// </summary>
        public const string RutaArchivoConsecutivo= "/home/user/apps/Consecutivo.txt";



        public const string RutaArchivoLinea = "/home/user/apps/Linea.txt";

        public const string RutaArchivoImagenes = "/home/user/apps/Imagenes.txt";
        
    }
}
