﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomatizacionEtiquetado.Datos.Fabricas
{
    /// <summary>
    /// Clase estática para la contención de las fábricas
    /// </summary>
    public static class Fabricas
    {
        /// <summary>
        /// Contiene la fábrica de usuarios
        /// </summary>
        private static FabricaUsuarios miFabricaUsuarios;

        /// <summary>
        /// Contiene la fábrica de números de parte
        /// </summary>
        private static FabricaNumeroParte miFabricaNumerosDeParte;

        /// <summary>
        /// Contiene la fábrica de configuración
        /// </summary>
        private static FabricaConfiguracion miFabricaConfiguracion;

        /// <summary>
        /// Da acceso a la fábrica de usuarios
        /// </summary>
        public static FabricaUsuarios fabricaUsuarios
        {
            get
            {
                if (miFabricaUsuarios == null)
                    miFabricaUsuarios = new FabricaUsuarios();
                return miFabricaUsuarios;
            }
            set
            {
                miFabricaUsuarios=value;
            }
        }

        /// <summary>
        /// Da acceso a la fábrica de números de parte
        /// </summary>
        public static FabricaNumeroParte fabricaNumerosParte
        {
            get
            {
                if (miFabricaNumerosDeParte == null)
                    miFabricaNumerosDeParte = new FabricaNumeroParte();
                return miFabricaNumerosDeParte;
            }
            set
            {
                miFabricaNumerosDeParte = value;
            }
        }

        

        /// <summary>
        /// Da acceso a la fábrica de configuración
        /// </summary>
        public static FabricaConfiguracion fabricaConfiguracion
        {
            get
            {
                if (miFabricaConfiguracion == null)
                    miFabricaConfiguracion = new FabricaConfiguracion();
                return miFabricaConfiguracion;
            }
            set
            {
                miFabricaConfiguracion = value;
            }
        }
    }
}
