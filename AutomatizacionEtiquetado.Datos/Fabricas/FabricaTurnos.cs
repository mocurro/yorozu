﻿using System;
using System.Collections.Generic;
using System.Text;
using AutomatizacionEtiquetado.Comun.Globales;
using System.IO;
using AutomatizacionEtiquetado.Comun.Constantes;

namespace AutomatizacionEtiquetado.Datos.Fabricas
{
    public class FabricaTurnos
    {
        public string[] ObtenerTurnos()
        {
            string[] resultado = null;
            try
            {
                if (!File.Exists(Constantes.RutaArchivoTurnos))
                {
                    string valoresDefault = string.Empty;
                    valoresDefault += string.Format("{0},{1},{2}\n", 1, "06:30", "14:29");
                    valoresDefault += string.Format("{0},{1},{2}\n", 2, "14:30", "21:29");
                    valoresDefault += string.Format("{0},{1},{2}\n", 3, "21:30", "06:29");

                    using (FileStream fileStream = File.Create(Constantes.RutaArchivoTurnos))
                    {
                        Byte[] texto = new UTF8Encoding(true).GetBytes(valoresDefault);
                        fileStream.Write(texto, 0, texto.Length);
                    }
                    resultado = new string[3];
                    resultado[0] = string.Format("{0},{1},{2}\n", 1, "06:30", "14:29");
                    resultado[1] = string.Format("{0},{1},{2}\n", 2, "14:30", "21:29");
                    resultado[2] = string.Format("{0},{1},{2}\n", 3, "21:30", "06:29");
                    return resultado;
                }
                else
                {
                    StreamReader streamReader = new StreamReader(Constantes.RutaArchivoTurnos);
                    resultado = File.ReadAllLines(Constantes.RutaArchivoTurnos);
                    streamReader.Close();
                    streamReader.Dispose();
                }
            }
            catch (Exception ex)
            {
                Globales.RegistrarEvento(string.Format("Hubo un error en la obtención de los turnos {0}", ex.Message), Automatizacion_de_etiquetado.Enumeradores.TiposMensajes.ErrorInesperado);
                return null;
            }
            return resultado;
        }
    }
}
