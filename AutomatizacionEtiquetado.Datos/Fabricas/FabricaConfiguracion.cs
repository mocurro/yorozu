﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using AutomatizacionEtiquetado.Comun.Constantes;
using AutomatizacionEtiquetado.Comun.Globales;

namespace AutomatizacionEtiquetado.Datos.Fabricas
{
    public class FabricaConfiguracion
    {
        public String[] ObtenerConfiguraciones()
        {
            String[] resultado = null;

            try
            {
                if (!File.Exists(Constantes.RutaArchivoConfiguracion))
                {
                    string cantidadPallet = string.Format("{0},{1}", Constantes.CantidadPallet, 60);
                    using (FileStream fileStream = File.Create(Constantes.RutaArchivoConfiguracion))
                    {
                        Byte[] texto = new UTF8Encoding(true).GetBytes(cantidadPallet);
                        fileStream.Write(texto, 0, texto.Length);
                    }
                    resultado = new string[1];
                    resultado[0] = cantidadPallet;
                }
                else
                {
                    StreamReader streamReader = new StreamReader(Constantes.RutaArchivoConfiguracion);
                    resultado = File.ReadAllLines(Constantes.RutaArchivoConfiguracion);
                    streamReader.Close();
                    streamReader.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                return new string[1];
            }
        }
    }
}
