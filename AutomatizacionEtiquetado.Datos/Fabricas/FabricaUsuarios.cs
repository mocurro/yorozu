﻿using System;
using System.Collections.Generic;
using System.Text;
using AutomatizacionEtiquetado.Datos.Clases;
using System.IO;
using AutomatizacionEtiquetado.Comun.Constantes;

namespace AutomatizacionEtiquetado.Datos.Fabricas
{
    /// <summary>
    /// Clase para la manipulación de los datos de usuario
    /// </summary>
    public class FabricaUsuarios
    {
        /// <summary>
        /// Devuelve la lista de usuarios
        /// </summary>
        public String[] ObtenerExistentes()
        {
            String[] resultado;
            try
            {
                if (!File.Exists(Constantes.RutaArchivoUsuarios))
                {
                    string usuarioSistema = "9945";
                    using (FileStream fileStream = File.Create(Constantes.RutaArchivoUsuarios))
                    {
                        Byte[] texto = new UTF8Encoding(true).GetBytes(usuarioSistema);
                        fileStream.Write(texto, 0, texto.Length);
                    }
                    resultado = new string[1];
                    resultado[0] = usuarioSistema;
                }
                else
                {
                    StreamReader streamReader = new StreamReader(Constantes.RutaArchivoUsuarios);
                    resultado = File.ReadAllLines(Constantes.RutaArchivoUsuarios);
                    streamReader.Close();
                    streamReader.Dispose();
                }
            }
            catch (Exception ex)
            {
                return new string[1];
            }
            return resultado;
        }
    }
}
