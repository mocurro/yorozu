﻿using System;
using System.Collections.Generic;
using System.Text;
using AutomatizacionEtiquetado.Datos.Clases;
using AutomatizacionEtiquetado.Comun;
using AutomatizacionEtiquetado.Comun.Globales;
using System.IO;
using AutomatizacionEtiquetado.Comun.Constantes;
using Automatizacion_de_etiquetado.Enumeradores;

namespace AutomatizacionEtiquetado.Datos.Fabricas
{
    /// <summary>
    /// Clase para la manipulación de datos de números de parte
    /// </summary>
    public class FabricaNumeroParte
    {

        
        
        
        /// <summary>
        /// Devuelve la lista de números de parte
        /// </summary>
        public String[] ObtenerNumerosDeParte()
        {
            try
            {
                String[] resultado = null;
                if (!File.Exists(Constantes.RutaArchivoNumerosDeParte))
                {
                    File.Create(Constantes.RutaArchivoNumerosDeParte);
                    return resultado;
                }
                else
                {
                    StreamReader streamReader = new StreamReader(Constantes.RutaArchivoNumerosDeParte);
                    int contador = 0;
                    resultado = File. ReadAllLines(Constantes.RutaArchivoNumerosDeParte);
                    //while (streamReader.Peek() > 0)
                    //{
                    //    resultado[contador] = streamReader.ReadLine();
                    //    contador++;
                    //}
                    streamReader.Close();
                    streamReader.Dispose();  
                }
                return resultado;
            }
            catch (Exception ex)
            {
                return new String[1];
            }
            
        }

        /// <summary>
        /// Devuelve la lista de números de parte
        /// </summary>
        public String ObtenerNumerosDeParte2()
        {
            try
            {
                String resultado = null;
                if (!File.Exists(Constantes.RutaArchivoNumerosDeParte))
                {
                    File.Create(Constantes.RutaArchivoNumerosDeParte);
                    return resultado;
                }
                else
                {
                    StreamReader streamReader = new StreamReader(Constantes.RutaArchivoNumerosDeParte);
                    int contador = 0;
                    //resultado = File.ReadAllLines(Constantes.RutaArchivoNumerosDeParte);
                    while (streamReader.Peek() > 0)
                    {
                        resultado = streamReader.ReadLine();
                        break;
                    }
                    streamReader.Close();
                    streamReader.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                return "";
            }

        }

        public String[] ObtenerTurnos() {
            try
            {
                String[] resultado = null;
                if (!File.Exists(Constantes.RutaArchivoNumerosDeParte))
                {
                    File.Create(Constantes.RutaArchivoNumerosDeParte);
                    return resultado;
                }
                else
                {
                    StreamReader streamReader = new StreamReader(Constantes.RutaArchivoNumerosDeParte);
                    int contador = 0;
                    resultado = File.ReadAllLines(Constantes.RutaArchivoNumerosDeParte);
                    //while (streamReader.Peek() > 0)
                    //{
                    //    resultado[contador] = streamReader.ReadLine();
                    //    contador++;
                    //}
                    streamReader.Close();
                    streamReader.Dispose();
                }
                return resultado;
            }
            catch (Exception ex)
            {
                return new String[1];
            }
        }

        public int ObtenerConsecutivo()
        {
            try
            {
                int resultado = 0;
                if (!File.Exists(Constantes.RutaArchivoConsecutivo))
                {
                    File.Create(Constantes.RutaArchivoConsecutivo);
                    return 0;
                }
                else
                {


                    StreamReader streamReader = new StreamReader(Constantes.RutaArchivoConsecutivo);
                    while (streamReader.Peek() > 0)
                    {
                        resultado = int.Parse ( streamReader.ReadLine());
                        if (resultado == 100000000) {
                            resultado = 1;
                        }
                        break;
                    }
                    streamReader.Close();
                    streamReader.Dispose();
                    
                }
                return resultado;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public String ObtenerLinea()
        {
            String resultado = null;
            try
            {

                if (!File.Exists(Constantes.RutaArchivoLinea))
                {
                    File.Create(Constantes.RutaArchivoLinea);
                    return resultado;
                }
                else
                {
                    StreamReader streamReader = new StreamReader(Constantes.RutaArchivoLinea);
                    resultado = streamReader.ReadLine();
                    //while (streamReader.Peek() > 0)
                    //{
                    //    resultado[contador] = streamReader.ReadLine();
                    //    contador++;
                    //}
                    streamReader.Close();
                    streamReader.Dispose();


                }
                return "1";
            }
            catch (Exception ex)
            {
                return resultado;
            }

        }

        public string AplicaionSeleccionada()
        {

            int request=20;
            String lectura = null;
            try
            {

                if (!File.Exists(Constantes.RutaArchivoConfiguracion))
                {
                    File.Create(Constantes.RutaArchivoConfiguracion);
                    return lectura;
                }
                else
                {
                    StreamReader streamReader = new StreamReader(Constantes.RutaArchivoConfiguracion);
                     lectura= streamReader.ReadLine();
                      streamReader.Close();
                      streamReader.Dispose();

                   
 


                }
                return lectura;
            }
            catch (Exception ex)
            {
                return lectura;
            }

        }
        public void guardarConsecutivo(int consecutivo) {         
            try
            {
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(Constantes.RutaArchivoConsecutivo,false );
                //Write a line of text
                sw.WriteLine(consecutivo);
                //Close the file
                sw.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }



        public String[] ObtenerImagenes()
        {
            String[] resultado = null;

            try
            {
                if (!File.Exists(Constantes.RutaArchivoImagenes))
                    return null;
                else
                    resultado = File.ReadAllLines(Constantes.RutaArchivoImagenes);
                if (resultado.Length == 0)
                    return null;
            }
            catch (Exception ex)
            {
                return null;
            }
            return resultado;
        }


    }
}
