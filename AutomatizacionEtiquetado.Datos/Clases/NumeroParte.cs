﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace AutomatizacionEtiquetado.Datos.Clases
{
    /// <summary>
    /// Clase para identificar los números de parte
    /// </summary>
    public class NumeroParte
    {

        /*<STX><ESC>C;<ESC>P;E1;F1;W10;o0,0;w2;l70;h393;L11;o35,0;w2;l393;f3;L12;o35,121;w2;l35;L13;o0,43;w2;l35;L14;o0,121;w2;l35;L15;o0,242;w2;l35;<ETX>  <STX>H0;o69,010;f3;h0;w0;c61;k9;d3, VW AG ;<ETX><STX>H1;o69,141;f3;h0;w0;c62;k9;d3, 5Q0820741B  ;<ETX><STX>H2;o30,005;f3;h0;w0;c61;k6;d3,     ;<ETX><STX>H3;o35,052;f3;h0;w0;c61;k9;d3, 10:02 ;<ETX><STX>H4;o35,129;f3;h0;w0;c61;k9;d3, 1  5 14/17;<ETX><STX>H5;o35,251;f3;h0;w0;c61;k9;d3, Mexico ZRP;<ETX><STX>R;<ESC>E1<CAN><ETB><FF><ETX>*/


        public string campo1 { get; set; }

        public string campo2 { get; set; }

        public string codigo { get; set; }

        public string hora { get; set; }

        public string fecha{ get; set; }

        public string Serial { get; set; }

        public string fechaunida
        {
            get
            {
                DateTime miFecha = DateTime.Now;
                //D02022020132301”
                string fechaManufactura = string.Format("{0}{1}{2}{3}{4}{5}", miFecha.Day.ToString().PadLeft(2, '0'), miFecha.Month.ToString().PadLeft(2, '0'), miFecha.Year.ToString(), miFecha.Hour.ToString().PadLeft(2, '0'), miFecha.Minute.ToString().PadLeft(2, '0'), miFecha.Second.ToString().PadLeft(2, '0'));
                return fechaManufactura;
            }
        }

        public string FechaFormato
        {
            get
            {
                DateTime miFecha = DateTime.Now;
                //D02022020132301”
                string fechaManufactura = string.Format("{0}/{1}/{2} {3}:{4}:{5}", miFecha.Day.ToString().PadLeft(2, '0'), miFecha.Month.ToString().PadLeft(2, '0'), miFecha.Year.ToString(), miFecha.Hour.ToString().PadLeft(2, '0'), miFecha.Minute.ToString().PadLeft(2, '0'), miFecha.Second.ToString().PadLeft(2, '0'));
                return fechaManufactura;
            }
        }

        public string pais { get; set; }

        public string campo3 { get; set; }

        public string maquina { get; set; }

        public string usuario { get; set; }
        public string linea { get; set; }

        public string cantidad { get; set; }

        private string datoslote { get; set; }

        public string turno { get; set; }

        public string lote
        {
            get
            {

                //if (datoslote == "")
                //{ 

                    string resultado = "";
                    // 1  5 14/17
                    // 1  5 14/17
                    // 1  1 28/17
                    DateTime miFecha = DateTime.Now;
                    char pad = '0'; 

                    string mes;
                    
                    switch (miFecha.Month )
                    {
                      case 10:
                         mes="X";
                          break;
                      case 11:
                           mes="Y";
                          break;

                     case 12:
                           mes="Z";
                          break;
                      default:
                          mes=miFecha.Month.ToString ();
                          break;
                    }

                    string dia = miFecha.Day.ToString().PadLeft(2, pad);

                    string turno;

                    if (miFecha.Hour >= 8 && miFecha.Hour <= 20)
                    {
                        turno = "1";
                        if (miFecha.Hour == 20)
                        {
                            if (miFecha.Minute <= 30)
                            {
                                turno = "1";
                            }
                            else
                            {
                                turno = "3";
                            }
                        }
                        
                    }
                    else {
                        turno = "3";
                    }
                    if (miFecha.Hour <= 8)
                    {
                        if (miFecha.Hour == 8)
                        {
                            if (miFecha.Minute == 0) {
                                dia = miFecha.AddDays(-1).Day.ToString().PadLeft(2, pad); 
                            }
                        }
                        else {
                            dia = miFecha.AddDays(-1).Day.ToString().PadLeft(2, pad); 
                        }                        
                    }
                    string hora;

                    hora=string.Format ("{0}:{1}",miFecha.Hour.ToString().PadLeft(2, '0'),miFecha.Minute.ToString().PadLeft(2, '0'));


                    datoslote = string.Format("{0}{1}{2}{3}{4}{5}", miFecha.Year.ToString().Substring(miFecha.Year.ToString().Length - 1, 1), mes, dia, turno, linea, hora);

                //}
                return datoslote;
            }
        }


        public NumeroParte()
        {
        }

        public NumeroParte(string cadenaSerial)
        {
            string[] separador = new string[]{"<STX>"};
            string[] cadenaSerializada = cadenaSerial.Split(separador,StringSplitOptions.None);
            try
            {
                campo1 = cadenaSerializada[2].Substring(cadenaSerializada[2].IndexOf("d3,")).Replace(";<ETX>", "").Replace("d3,",string.Empty);
            }
            catch
            {
                campo1 = "VW AG";
            }

            codigo = cadenaSerializada[3].Substring(cadenaSerializada[3].IndexOf("d3,")).Replace(";<ETX>", "").Replace("d3,", string.Empty).Trim();
            

            try 
            {
                hora = cadenaSerializada[5].Substring(cadenaSerializada[5].IndexOf("d3,")).Replace(";<ETX>", "").Replace("d3,", string.Empty).Trim();
            }
            catch
            {
                DateTime horaActual = DateTime.Now;
                hora = horaActual.Date.ToString("hh:mm");
            }

            try
            {
                fecha = cadenaSerializada[6].Substring(cadenaSerializada[6].IndexOf("d3,")).Replace(";<ETX>", "").Replace("d3,", string.Empty).Trim();
            }
            catch
            {
                DateTime fechaActual = DateTime.Now;
                fecha = "1" + fechaActual.Date.ToString("mm dd/yy");
            }
            

            
            //campo2 = cadenaSerializada[4].Substring(cadenaSerializada[4].IndexOf(' ')).Replace(";<ETX>", "").Trim();

            try
            {
                pais = cadenaSerializada[7].Substring(cadenaSerializada[7].IndexOf("d3,")).Replace(";<ETX>", "").Replace("d3,", string.Empty).Trim();
            }
            catch
            {
                pais = "Mexico ZRP";
            }
            
           // campo3 = cadenaSerializada[8].Substring(0, cadenaSerializada[8].IndexOf(';')).Trim();
        }

        public NumeroParte(string NoParte, string MiMaquina, string miUsuario)
        {
            string []dato=NoParte.Split(new Char[] {','});

            codigo = dato[0];
            cantidad = dato[1];
            DateTime horaActual = DateTime.Now;
            hora = horaActual.Date.ToString("hh:mm");            
            fecha = horaActual.Date.ToString("yyy/MM/dd");
            pais = "Mexico ZRP";
            linea = MiMaquina;
            usuario = miUsuario;
        }

        public NumeroParte(string NoParte,string miserial)
        {
            codigo = NoParte;
            DateTime horaActual = DateTime.Now;
            hora = horaActual.Date.ToString("hh:mm");
            fecha = horaActual.Date.ToString("yyy/MM/dd");
            pais = "Mexico ZRP";
            Serial = miserial;
        }

        public string CodigoDataMatrix
        {
            get
            {
                string ceros = "9999999999";
                string Supplier = "1418200000";
                string manufactura = string.Format("P{0}Q{1}T{2}D{3}V51974002", codigo, cantidad, lote, fechaunida);
                //string manufactura = string.Format("{0}{1}{2}{3}", codigo, ceros, Serial, Supplier);
                
                return manufactura;
            }
        }


    }
}
