﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomatizacionEtiquetado.Datos.Clases
{
    public class Turno
    {
        public string valor { get; set; }
        public TimeSpan inicio { get; set; }
        public TimeSpan fin { get; set; }

        public Turno(string valor, string inicio, string fin)
        {
            this.valor = valor;
            this.inicio = TimeSpan.Parse(inicio);
            this.fin = TimeSpan.Parse(fin);
        }
    }
}
