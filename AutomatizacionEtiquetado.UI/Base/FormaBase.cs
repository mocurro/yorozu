﻿using System;
using System.Collections.Generic;
using System.Text;
using Intermec.ICPUIElements;
using Intermec.Printer;
using Automatizacion_de_etiquetado.Enumeradores;
using AutomatizacionEtiquetado.Comun.Globales;
using System.Reflection;
using AutomatizacionEtiquetado.Datos.Clases;
using AutomatizacionEtiquetado.Comun.Clases;

namespace AutomatizacionEtiquetado.UI.Base
{
    public class FormaBase : Form
    {
        /// <summary>
        /// Control para representar la etiqueta que contiene el título de la aplicación
        /// </summary>
        private TextLabel etiquetaTitulo;
        private TextLabel etiquetVersion;

        ///////// <summary>
        ///////// Control para representar la etiqueta que contiene la versión de la aplicación
        ///////// </summary>
        //////private TextLabel etiquetVersion;

        /// <summary>
        /// Constructor de la forma
        /// </summary>
        public FormaBase(string nombreForma, Color color, string titulo): base(nombreForma, color)
        {
            Globales.RegistrarEvento("Iniciando aplicacion",TiposMensajes.Aviso);
            etiquetaTitulo = Controles.GenerarEtiqueta(nombreForma, "FormTitle", 5, 3, "     Sistema de Etiquetado");
            etiquetVersion = Controles.GenerarEtiqueta(nombreForma, "Versión", 5, 20, " Versión 7.0.0");
        }

        /// <summary>
        /// Obtiene la versión de la aplicación
        /// </summary>
        /// <returns>Cadena de texto con la versión de la aplicación</returns>
        private string ObtenerVersion()
        {
            string version = "";
            try
            {
                Assembly ensamblado = System.Reflection.Assembly.GetExecutingAssembly();
                version = String.Format("Versión {0}.{1}.{2}", ensamblado.GetName().Version.Major, ensamblado.GetName().Version.Minor, ensamblado.GetName().Version.Build);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error al obtener la versión de la aplicación " + ex.Message);
                version = "0";
            }
            return version;
        }

        /// <summary>
        /// Genera una plantilla de tipo Drawing para enviar a la impresión
        /// </summary>
        /// <param name="numeroParte">El número de parte involucrado</param>
        /// <returns>Objeto de tipo imagen</returns>
        public Drawing GenerarPlantilla(NumeroParte numeroParte)
        {
            Drawing resultado = new Drawing(400,115);
            Drawing.Text imagenCodigo = new Drawing.Text(170, 40, "Univers Bold",numeroParte.codigo);
            Drawing.Text imagenCliente = new Drawing.Text(15, 40, "Univers", numeroParte.campo1);
            Drawing.Text imagenHora = new Drawing.Text(40, 2, "Univers",numeroParte.hora);
            Drawing.Text imagenFecha = new Drawing.Text( 110, 2, "Univers", numeroParte.fecha);
            Drawing.Text imagenPais = new Drawing.Text(240, 2, "Univers", numeroParte.pais);
            
            Drawing.Rectangle rectangulo = new Drawing.Rectangle(3, 3, 397, 75);
            Drawing.Line lineaMedia = new Drawing.Line();
            Drawing.Line lineaMediaHorizontal = new Drawing.Line();
            Drawing.Line mediaLineaMediaHorizontal = new Drawing.Line();
            Drawing.Line mediaLineaMediaHorizontalDerecha = new Drawing.Line();

            rectangulo.LineWidth = 1;
            
            lineaMedia.Point = new Point(3,39);
            lineaMedia.EndPoint = new Point(397, 39);
            lineaMedia.LineWidth = 1;

            lineaMediaHorizontal.Point = new Point(108, 3);
            lineaMediaHorizontal.EndPoint = new Point(108, 75);
            lineaMediaHorizontal.LineWidth = 1;

            mediaLineaMediaHorizontal.Point = new Point(38, 3);
            mediaLineaMediaHorizontal.EndPoint = new Point(38, 39);
            mediaLineaMediaHorizontal.LineWidth = 1;

            mediaLineaMediaHorizontalDerecha.Point = new Point(238, 3);
            mediaLineaMediaHorizontalDerecha.EndPoint = new Point(238, 39);
            mediaLineaMediaHorizontalDerecha.LineWidth = 1;

            imagenCodigo.Height = 9;
            imagenCliente.Height = 9;
            imagenHora.Height = 9;
            imagenFecha.Height = 9;
            imagenPais.Height = 9;

            resultado += imagenCodigo;
            resultado += imagenCliente;
            resultado += imagenHora;
            resultado += imagenFecha;
            resultado += imagenPais;
            
            resultado += lineaMedia;
            resultado += lineaMediaHorizontal;
            resultado += mediaLineaMediaHorizontal;
            resultado += mediaLineaMediaHorizontalDerecha;

            resultado += rectangulo;
            return resultado;
        }

        public Dictionary<string, string> VariablesImpresion(NumeroParte numeroParte)
        {
            Dictionary<string, string> resultado = new Dictionary<string, string>();

            resultado.Add("$NOPARTE$", numeroParte.codigo);
            resultado.Add("$CANTIDAD$", numeroParte.cantidad);
            resultado.Add("$LOTE$", numeroParte.lote);
            resultado.Add("$FECHA$", numeroParte.FechaFormato);//@David Gamez Revisar formato de fecha correcto
            resultado.Add("$CodQR$", numeroParte.CodigoDataMatrix);

            //resultado.Add("$CODIGO$", numeroParte.codigo + numeroParte.linea + numeroParte.fecha);
            //resultado.Add("$NUMERO_PARTE$", numeroParte.codigo);
            //resultado.Add("$LINEA$", "Línea: "+ numeroParte.linea);
            //resultado.Add("$CAMPO1$", numeroParte.campo1);
            //resultado.Add("$HORA$", numeroParte.hora);
            //resultado.Add("$FECHA$", numeroParte.fecha);
            //resultado.Add("$PAIS$", numeroParte.pais);
            //resultado.Add("$2D", numeroParte.CodigoDataMaxtrix);
            //resultado.Add("$ITEM$", numeroParte.codigo );
            //resultado.Add("$MAQUINA$", numeroParte.maquina );
            //resultado.Add("$DESC$", numeroParte.codigo);
            //resultado.Add("$OPERADOR$", numeroParte.usuario );
            return resultado;
        }
    }
}
