﻿using System;
using System.Collections.Generic;
using System.Text;
using Intermec.ICPUIElements;
using Intermec.Printer;
using Automatizacion_de_etiquetado.Enumeradores;
using AutomatizacionEtiquetado.Comun.Globales;

namespace AutomatizacionEtiquetado.UI.Base
{
    /// <summary>
    /// Forma diseñada para mensajes emergentes al usuario
    /// </summary>
    class FormaMensaje : Form, IDisposable
    {
        /// <summary>
        /// Delegado para la confirmación de la ventana
        /// </summary>
        public delegate void FormaMensaje_Confirmacion();

        /// <summary>
        /// Delegado para la cancelación de la ventana
        /// </summary>
        public delegate void FormaMensaje_Cancelacion();

        /// <summary>
        /// Evento que se ejecuta cuando se confirma la acción
        /// </summary>
        public event FormaMensaje_Confirmacion Confirmacion;

        /// <summary>
        /// Delegado para la cancelación de la ventana
        /// </summary>
        public event FormaMensaje_Cancelacion Cancelacion;


        /// <summary>
        /// Control de encabezado
        /// </summary>
        TextLabel etiquetaEncabezado;

        /// <summary>
        /// Control para identificar la etiqueta del mensaje o pregunta
        /// </summary>
        TextMultlineLabel etiquetaPregunta;

        /// <summary>
        /// Botón aceptar
        /// </summary>
        Button botonAceptar;

        /// <summary>
        /// Botón cancelar
        /// </summary>
        Button botonCancelar;



        /// <summary>
        /// Constructor de la forma, inicializa los valores mínimso
        /// </summary>
        public FormaMensaje(string nombreForma, Color color)
            : base(nombreForma, color)
        {
            GenerarControles(string.Empty, string.Empty, TiposMensajes.Aviso);
            botonAceptar = Controles.GenerarBoton(this.Name, "btnAceptar", 10, 280, Button.BtnStyle.RectHalf, "Aceptar");
            botonCancelar = Controles.GenerarBoton(this.Name, "btnCancelar", 125, 280, Button.BtnStyle.RectHalf, "Cancelar");

            
            botonAceptar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(botonAceptar_Click);
            botonCancelar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(botonCancelar_Click);
        }

        /// <summary>
        /// Muestra un mensaje al usuario
        /// </summary>
        /// <param name="encabezado">El encabezado del mensaje</param>
        /// <param name="mensaje">El mensaje a mostrar</param>
        /// <param name="tipoMensaje">El tipo de mensaje</param>
        public void MostrarMensaje(string encabezado, string mensaje, TiposMensajes tipoMensaje)
        {
            botonCancelar.Visible = false;
            botonAceptar.Style = Button.BtnStyle.RectFull;
            etiquetaEncabezado.Text = encabezado;
            etiquetaPregunta.Text = mensaje;
            base.Show();
        }

       

        /// <summary>
        /// Realiza una pregunta al usuario
        /// </summary>
        /// <param name="encabezado">El encabezado de la pregunta</param>
        /// <param name="mensaje">La pregunta a mostrar</param>
        public void Preguntar(string encabezado, string mensaje)
        {
            //GenerarControles(encabezado, mensaje,TiposMensajes.Aviso);
            //botonAceptar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(botonAceptar_Click);
            //botonCancelar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(botonCancelar_Click);
            etiquetaEncabezado.Text = encabezado;
            etiquetaPregunta.Text = mensaje;
            base.Show();
        }

        /// <summary>
        /// Genera los controles visuales
        /// </summary>
        /// <param name="encabezado">El encabezado de la forma</param>
        /// <param name="mensaje">El mensaje a mostrar</param>
        public void GenerarControles(string encabezado, string mensaje, TiposMensajes tipoMensaje)
        {
            etiquetaEncabezado = Controles.GenerarEtiqueta(this.Name, "lblEncabezado", 10, 10, encabezado, 18, ICPUIClr.clrWhite);
            etiquetaPregunta = Controles.GenerarEtiquetaMultilinea(this.Name, "lblMensaje", 10, 30, 200, "Univers", 18, mensaje);
        }


        /// <summary>
        /// Se presiona el botón aceptar indicando el resultado
        /// </summary>
        private void botonAceptar_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
        {
            Close();
            Confirmacion();
        }
       

        /// <summary>
        /// Se presiona el botón cancelar indicando el resultado
        /// </summary>
        private void botonCancelar_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
        {
            Close();
            Cancelacion();
        }





        #region IDisposable Members

        public void Dispose()
        {
            this.Close();
        }

        #endregion
    }
}
