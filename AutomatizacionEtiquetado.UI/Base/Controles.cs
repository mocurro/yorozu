﻿using System;
using System.Collections.Generic;
using System.Text;
using Intermec.ICPUIElements;
using Intermec.Printer;

namespace AutomatizacionEtiquetado.UI.Base
{
    /// <summary>
    /// Clase para la generación de los controles de la aplicación
    /// </summary>
    public static class Controles
    {
        /// <summary>
        /// Genera una etiqueta
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma en la que se invoca</param>
        /// <param name="propiedad">La propiedad</param>
        /// <param name="x">La posición en x</param>
        /// <param name="y">La posición en y</param>
        /// <param name="texto">El texto a mostrar</param>
        /// <param name="color">El color del texto</param>
        /// <param name="fuente">El tipo de letra a utilizar</param>
        /// <returns>Objeto de tipo etiqueta</returns>
        public static TextLabel GenerarEtiqueta(string nombreForma, string propiedad, int x, int y, string texto, Color color, string fuente)
        {
            TextLabel resultado = new TextLabel(nombreForma, propiedad, x, y, texto);
            resultado.FontName = fuente;
            resultado.ForeColor = color;
            return resultado;
        }

        /// <summary>
        /// Genera una etiqueta
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma en la que se invoca</param>
        /// <param name="propiedad">La propiedad</param>
        /// <param name="x">La posición en x</param>
        /// <param name="y">La posición en y</param>
        /// <param name="texto">El texto a mostrar</param>
        /// <returns>Objeto de tipo etiqueta</returns>
        /// <remarks>Etiqueta con texto en negro y fuente Univers Bold</remarks>
        public static TextLabel GenerarEtiqueta(string nombreForma, string propiedad, int x, int y, string texto)
        {
            return GenerarEtiqueta(nombreForma, propiedad, x, y, texto, ICPUIClr.clrBlack, "Univers Bold");
        }


        /// <summary>
        /// Genera una etiqueta
        /// </summary>
        /// <param name="nombreForma">El nombre del forma en la que se invoca</param>
        /// <param name="nombreControl">El nombre del control</param>
        /// <param name="x">La posición en x</param>
        /// <param name="y">La posición en y</param>
        /// <param name="texto">El texto a mostrar</param>
        /// <param name="tamañoFuente">El tamaño de la fuente</param>
        /// <param name="color">El color de la fuente</param>
        /// <returns></returns>
        public static TextLabel GenerarEtiqueta(string nombreForma, string nombreControl, int x, int y, string texto, int tamañoFuente,Color color)
        {
            return new TextLabel(nombreForma, nombreControl, x, y, texto, tamañoFuente, color);
        }

        /// <summary>
        /// Genera una etiqueta
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma en la que se invoca</param>
        /// <param name="nombre">El nombre del control</param>
        /// <param name="x">La posición en x</param>
        /// <param name="y">La posición en y</param>
        /// <param name="ancho">El ancho del control</param>
        /// <param name="fuente">La fuente a utilizar</param>
        /// <param name="tamañoFuente">El tamaño de la fuente</param>
        /// <param name="texto">El texto a mostrar</param>
        /// <returns>Un objeto de tipo TextMultlineLabel</returns>
        public static TextMultlineLabel GenerarEtiquetaMultilinea(string nombreForma, string nombre, int x, int y,int ancho,string fuente,int tamañoFuente, string texto)
        {
            TextMultlineLabel resultado = new TextMultlineLabel(nombreForma, nombre, x, y, ancho, fuente, tamañoFuente, texto);
            resultado.Enabled = false;
            return resultado;
        }

        /// <summary>
        /// Genera una etiqueta
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma en la que se invoca</param>
        /// <param name="nombre">El nombre del control</param>
        /// <param name="x">La posición en x</param>
        /// <param name="y">La posición en y</param>
        /// <param name="texto">El texto a mostrar</param>
        /// <returns>Un objeto de tipo TextMultlineLabel</returns>
        public static TextMultlineLabel GenerarEtiquetaMultilinea(string nombreForma, string nombre, int x, int y, string texto)
        {
            return GenerarEtiquetaMultilinea(nombre, nombre, x, y, 200, "Univers Bold", 12, texto);
        }

        /// <summary>
        /// Genera una lista desplegable
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma que la invoka</param>
        /// <param name="propiedad">El nombre del control</param>
        /// <param name="x">La coordenada en X</param>
        /// <param name="y">La coordenada en Y</param>
        /// <param name="ancho">El ancho del control</param>
        /// <param name="alto">El alto del control</param>
        /// <param name="fuenteDatos">La lista de datos a mostrar</param>
        /// <param name="encabezado">El encabezado a mostrar</param>
        /// <returns>Regresa un objeto de lista desplegable</returns>
        public static DataGrid GenerarListaDesplegable(string nombreForma, string propiedad, int x, int y, int ancho, int alto, string[] fuenteDatos, string encabezado)
        {
            return new DataGrid(nombreForma, propiedad, x, y, ancho, alto, fuenteDatos, encabezado);
        }


        /// <summary>
        /// Genera una lista desplegable
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma que la invoka</param>
        /// <param name="propiedad">El nombre del control</param>
        /// <param name="x">La coordenada en X</param>
        /// <param name="y">La coordenada en Y</param>
        /// <param name="ancho">El ancho del control</param>
        /// <param name="alto">El alto del control</param>
        /// <param name="fuenteDatos">La lista de datos a mostrar</param>
        /// <returns>Regresa un objeto de lista desplegable</returns>
        public static DataGrid GenerarListaDesplegable(string nombreForma, string propiedad, int x, int y, int ancho, int alto, string[] fuenteDatos)
        {
            return GenerarListaDesplegable(nombreForma, propiedad, x, y, ancho, alto, fuenteDatos,string.Empty);
        }

        /// <summary>
        /// Genera una lista desplegable
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma que la invoka</param>
        /// <param name="propiedad">El nombre del control</param>
        /// <param name="x">La coordenada en X</param>
        /// <param name="y">La coordenada en Y</param>
        /// <param name="fuenteDatos">La lista de datos a mostrar</param>
        /// <param name="encabezado">El encabezado a mostrar</param>
        /// <returns>Regresa un objeto de lista desplegable</returns>
        public static DataGrid GenerarListaDesplegable(string nombreForma, string propiedad, int x, int y, string[] fuenteDatos,string encabezado)
        {
            return GenerarListaDesplegable(nombreForma, propiedad, x, y, 225, 190, fuenteDatos,encabezado);
        }

        /// <summary>
        /// Genera una lista desplegable
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma que la invoka</param>
        /// <param name="propiedad">El nombre del control</param>
        /// <param name="x">La coordenada en X</param>
        /// <param name="y">La coordenada en Y</param>
        /// <param name="fuenteDatos">La lista de datos a mostrar</param>
        /// <returns>Regresa un objeto de lista desplegable</returns>
        public static DataGrid GenerarListaDesplegable(string nombreForma, string propiedad, int x, int y, string[] fuenteDatos)
        {
            return GenerarListaDesplegable(nombreForma, propiedad, x, y, 225, 190, fuenteDatos);
        }

        /// <summary>
        /// Genera un objeto de tipo botón
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma que lo invoca</param>
        /// <param name="propiedad">El nombre del botón</param>
        /// <param name="x">Coordenada en X</param>
        /// <param name="y">Coordenada en Y</param>
        /// <param name="texto">El texto a mostrar en el botón</param>
        /// <returns>Devuelve un objeto de tipo botón</returns>
        public static Button GenerarBoton(string nombreForma, string propiedad, int x, int y, string texto)
        {
            return new Button(nombreForma, propiedad, x, y, texto);
        }

        /// <summary>
        /// Genera un objeto de tipo botón
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma que lo invoca</param>
        /// <param name="propiedad">El nombre del botón</param>
        /// <param name="x">Coordenada en X</param>
        /// <param name="y">Coordenada en Y</param>
        /// <param name="estilo">El estilo del botón</param>
        /// <param name="texto">El texto a mostrar en el botón</param>
        /// <returns>Devuelve un objeto de tipo botón</returns>
        public static Button GenerarBoton(string nombreForma, string propiedad, int x, int y,Intermec.ICPUIElements.Button.BtnStyle estilo,string texto)
        {
            return new Button(nombreForma,propiedad,x,y,texto,estilo);
        }

        /// <summary>
        /// Genera un objeto de tipo caja de texto
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma que lo invoca</param>
        /// <param name="propiedad">El nombre de la caja de texto</param>
        /// <param name="x">La coordenada en X</param>
        /// <param name="y">La coordenada en Y</param>
        /// <param name="textoDefault">El texto precargado</param>
        /// <returns>Devuelve un objeto de tipo caja de texto</returns>
        public static TextBox GenerarCajaDeTexto(string nombreForma, string propiedad, int x, int y, string textoDefault)
        {
            return GenerarCajaDeTexto(nombreForma, propiedad, x, y, textoDefault, 200);
        }

        /// <summary>
        /// Genera un objeto de tipo caja de texto
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma que lo invoca</param>
        /// <param name="propiedad">El nombre de la caja de texto</param>
        /// <param name="x">La coordenada en X</param>
        /// <param name="y">La coordenada en Y</param>
        /// <returns>Devuelve un objeto de tipo caja de texto</returns>
        public static TextBox GenerarCajaDeTexto(String nombreForma, string propiedad, int x, int y)
        {
            return GenerarCajaDeTexto(nombreForma, propiedad, x, y, string.Empty);
        }

        /// <summary>
        /// Genera un objeto de tipo caja de texto
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma que lo invoca</param>
        /// <param name="propiedad">El nombre de la caja de texto</param>
        /// <param name="x">La coordenada en X</param>
        /// <param name="y">La coordenada en Y</param>
        /// <param name="textoDefault">El texto precargado</param>
        /// <param name="tamaño">El tamaño del control</param>
        /// <returns>Devuelve un objeto de tipo caja de texto</returns>
        public static TextBox GenerarCajaDeTexto(string nombreForma, string propiedad, int x, int y, string textoDefault, int tamaño)
        {
            return new TextBox(nombreForma, propiedad, x, y, textoDefault, tamaño);
        }
    }
}
