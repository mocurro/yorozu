﻿using System;
using System.Collections.Generic;
using System.Text;
using Intermec.ICPUIElements;
using Intermec.Printer;

namespace AutomatizacionEtiquetado.UI.Base
{
    public class FormaVacia:Form
    {
        public FormaVacia(string nombreForma, Color color)
            : base(nombreForma, color)
        {
        }

        public void Mostrar()
        {
            base.Show();
        }

        public void Cerrar()
        {
            base.Close();
        }
    }
}
