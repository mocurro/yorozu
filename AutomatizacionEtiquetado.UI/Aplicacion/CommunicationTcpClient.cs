using Intermec.Printer;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

class TestCommunicationTcpClient
{
	static bool tcpEchoServiceRunning = false;

    Thread thread;
	
    static int Main(string[] args)
    {
		string sendBuffer, recvBuffer;
		
        Console.WriteLine("CommunicationTcpClient");

		// Set up TCP listener echo service
		Thread thread = new Thread(new ThreadStart(TcpEchoService));
		thread.Start();
		
		// Wait for service to be started
		while(!tcpEchoServiceRunning)
		{
			Thread.Sleep(1000);
		}
		
		// Send a message to service and check reply
		sendBuffer = "the quick brown fox jumps over the lazy dog";
		Console.WriteLine("Main            send {0} bytes: {1}", 
		                  sendBuffer.Length, sendBuffer);
		recvBuffer = TcpSend(sendBuffer, "127.0.0.1", 8888);
		Console.WriteLine("Main            recv {0} bytes: {1}", 
		                  recvBuffer.Length, recvBuffer);

		// Stop TCP listener echo service
		tcpEchoServiceRunning = false;
		thread.Join();

        return 0;
	}

    public void Inicializar() {
       
        Console.WriteLine("CommunicationTcpClient");

        // Set up TCP listener echo service
        thread = new Thread(new ThreadStart(TcpEchoService));
        thread.Start();

        // Wait for service to be started
        while (!tcpEchoServiceRunning)
        {
            Thread.Sleep(1000);
        }
    
    }

    public string SendData(string Data) {
        string sendBuffer, recvBuffer;

        // Send a message to service and check reply
        sendBuffer = "the quick brown fox jumps over the lazy dog";
        Console.WriteLine("Main            send {0} bytes: {1}",
                          sendBuffer.Length, sendBuffer);
        recvBuffer = TcpSend(sendBuffer, "127.0.0.1", 8888);
        Console.WriteLine("Main            recv {0} bytes: {1}",
                          recvBuffer.Length, recvBuffer);

        // Stop TCP listener echo service
        tcpEchoServiceRunning = false;
        thread.Join();

        return recvBuffer;
    }


	private static void TcpEchoService()
	{
		// Set up a TCP listener for specified port on any address
		Communication.TcpListener tcpListener
			= new Communication.TcpListener(IPAddress.Any, 8888);
		tcpListener.Start();

		tcpEchoServiceRunning = true;
		int timeout = 0;
		
		while(tcpEchoServiceRunning && (timeout < 10))
		{
			// Check if there are any pending connections
			if(tcpListener.Pending())
			{
				Byte[] bytes = new Byte[256];
				int i;
				// Accept client
				TcpClient tcpClient = tcpListener.AcceptTcpClient();
				NetworkStream networkStream = tcpClient.GetStream();
				// Read and echo back data as long as there is data
				while(tcpEchoServiceRunning && 
							(i = networkStream.Read(bytes, 0, bytes.Length))!=0)
				{
					string data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
					Console.WriteLine("TcpEchoService  recv {0} bytes: {1}",
					                  data.Length, data);
					networkStream.Write(bytes, 0, i);
					Console.WriteLine("TcpEchoService  sent {0} bytes: {1}",
					                  data.Length, data);
				}
				// Close stream and connection
				networkStream.Close();
				tcpClient.Close();
				timeout = 0;
			}
			else
			{
				Thread.Sleep(1000);
				timeout++;
			}
		}
	}

	private static string TcpSend(string sendBuffer, string server, int port)
	{
		string recvBuffer = string.Empty;
		Byte[] recvData;
		Byte[] sendData;
		int bytesRead;
		int dataReceiveTimeout = 1000;
		
		try
		{
			// Open connection
			Communication.TcpClient tcpClient = 
				new Communication.TcpClient(server, port);
			sendData = System.Text.Encoding.ASCII.GetBytes(sendBuffer);
			NetworkStream networkStream = tcpClient.GetStream();
			
			// Send data
			networkStream.Write(sendData, 0, sendData.Length);
			
			// Receive data
			while(dataReceiveTimeout > 0)
			{
				if(networkStream.DataAvailable)
				{
					recvData = new Byte[256];
					bytesRead = networkStream.Read(recvData, 0, recvData.Length);
					recvBuffer += System.Text.Encoding.ASCII.GetString(recvData, 0, 
																														 bytesRead);
					dataReceiveTimeout = 1000;
				}			
				else
				{
					Thread.Sleep(100);
					dataReceiveTimeout -= 100;
				}
			}
			
			// Close connection
			networkStream.Close();
			tcpClient.Close();
		}
		catch(Exception e)
		{
			Console.WriteLine("Exception: {0}", e.Message);
		}
		
		return recvBuffer;
	}
}

