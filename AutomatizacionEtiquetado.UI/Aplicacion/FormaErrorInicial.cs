﻿using System;
using System.Collections.Generic;
using System.Text;
using AutomatizacionEtiquetado.UI.Base;
using Intermec.Printer;
using AutomatizacionEtiquetado.Comun.Globales;
using Automatizacion_de_etiquetado.Enumeradores;
using AutomatizacionEtiquetado.Datos.Clases;
using AutomatizacionEtiquetado.Datos.Fabricas;
using Intermec.ICPUIElements;
using AutomatizacionEtiquetado.Comun.Clases;
using System.Threading;
using AutomatizacionEtiquetado.Comun.Constantes;
using System.Globalization;

    public class FormaErrorInicial : FormaBase, IDisposable
    {

        TextLabel etiquetaClave;
        TextLabel etiquetaError;
        Button botonSalir;



        public FormaErrorInicial(string nombreForma, Color color,string error)
            : base(nombreForma, color, "")
        {
            etiquetaClave = Controles.GenerarEtiqueta(this.Name, "lblEtiqueta", 10, 50, "Error: ");
            etiquetaError = Controles.GenerarEtiqueta(this.Name, "lblEtiqueta", 10, 70, error);
            botonSalir = Controles.GenerarBoton(this.Name, "botonSalir", 10, 280, Button.BtnStyle.RectHalf, "Salir");
            botonSalir.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(BotonSalir_Click);

        }

        private void BotonSalir_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
        {
            this.Close();
        }

        public void MostrarPantalla()
        {
            try
            {
                base.Show();
            }catch(Exception ex){}
        }

        #region IDisposable Members

        public void Dispose()
        {
            this.Close();
        }

        #endregion
    

}
