﻿using System;
using System.Collections.Generic;
using System.Text;
using Intermec.ICPUIElements;
using Intermec.Printer;
using AutomatizacionEtiquetado.UI.Base;
using AutomatizacionEtiquetado.Datos.Fabricas;
using AutomatizacionEtiquetado.Comun.Globales;
using System;
using System.Net;
using System.IO;


namespace AutomatizacionEtiquetado.UI.Aplicacion
{

    /// <summary>
    /// Forma para verificación de usuarios
    /// </summary>
    public class FormaSesion: Form, IDisposable
    {

        /// <summary>
        /// Delegado para la confirmación de la ventana
        /// </summary>
        public delegate void FormaSesion_Confirmacion();

        /// <summary>
        /// Delegado para la cancelación de la ventana
        /// </summary>
        public delegate void FormaSesion_Cancelacion();

        /// <summary>
        /// Evento que se ejecuta cuando se confirma la acción
        /// </summary>
        public event FormaSesion_Confirmacion Confirmacion;

        /// <summary>
        /// Evento que ocurre cuando el usuario cancela la operación
        /// </summary>
        public event FormaSesion_Cancelacion Cancelacion;

        /// <summary>
        /// Botón aceptar
        /// </summary>
        Button botonAceptar;

        /// <summary>
        /// Botón cancelar
        /// </summary>
        Button botonCancelar;

        /// <summary>
        /// Control de encabezado
        /// </summary>
        TextLabel etiquetaEncabezado;

        /// <summary>
        /// Control para identificar la etiqueta del mensaje
        /// </summary>
        TextMultlineLabel etiquetaMensajeGeneral;

        /// <summary>
        /// Etiqueta de la clave de usuario
        /// </summary>
        TextLabel etiquetaClave;

        /// <summary>
        /// Caja de texto de la clave de usuario
        /// </summary>
        TextBox cajaClave;

        /// <summary>
        /// Etiqueta que muestra el resultado de la validación del usuario
        /// </summary>
        TextMultlineLabel etiquetaResultadoVerificacion;

        /// <summary>
        /// Contiene la lista de usuarios
        /// </summary>
        private String[] usuarios;

        private string contrasenaIngresada = string.Empty;

        /// <summary>
        /// Forma ulitzada para realizar la validación de la sesión de usuario
        /// </summary>
        FormaPrincipal miFormaPrincipal;
        FormaSinComparacion miFormaSinComparacion;

        TestCommunicationTcpClient Intancia;

        FormaPrincipal formaPrincipal
        {
            get
            {
                if (miFormaPrincipal == null)
                {
                    miFormaPrincipal = new FormaPrincipal("Principal", Globales.ColorAplicacion());
                    miFormaPrincipal.MostrarPantalla();
                    //FormaPrincipal formaPrincipal = new FormaPrincipal("Principal", Globales.ColorAplicacion());
                    //formaPrincipal.MostrarPantalla();
                }
                return miFormaPrincipal;
            }
            set
            {
                miFormaPrincipal = value;
            }
        }

        /// <summary>
        /// Constructor de la forma que carga los prerequisitos
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma a mostrar</param>
        /// <param name="color">El color que será usado de fondo</param>
        public FormaSesion(string nombreForma, Color color)
            : base(nombreForma, color)
        {

            CargarUsuarios();

            Intancia = new TestCommunicationTcpClient();
            etiquetaClave = Controles.GenerarEtiqueta(this.Name, "lblEtiqueta", 20, 100, "Clave de usuario");
            cajaClave = Controles.GenerarCajaDeTexto(this.Name, "txtClave", 20, 130);
            cajaClave.Enabled = false;

            etiquetaResultadoVerificacion = Controles.GenerarEtiquetaMultilinea(this.Name, "lblResultado", 5, 220, 220, "Univers Bold", 16, string.Empty);
            etiquetaResultadoVerificacion.ForeColor = Globales.ColorAplicacionError();

            etiquetaEncabezado = Controles.GenerarEtiqueta(this.Name, "lblEncabezado", 10, 10, "", 18, ICPUIClr.clrBlack);
            etiquetaMensajeGeneral = Controles.GenerarEtiquetaMultilinea(this.Name, "lblMensaje", 10, 30, 200, "Univers", 16, "Ingrese su clave para salir");

            botonAceptar = Controles.GenerarBoton(this.Name, "btnAceptar", 10, 280, Button.BtnStyle.RectHalf, "Aceptar");
            botonCancelar = Controles.GenerarBoton(this.Name, "btnCancelar", 125, 280, Button.BtnStyle.RectHalf, "Cancelar");

            botonAceptar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(botonAceptar_Click);
            botonCancelar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(botonCancelar_Click);

            this.KeyPress += new Intermec.Printer.UI.Keypad.KeyEventHandler(SePresiontaTecla);
            //Intancia.Inicializar();
        }

        /// <summary>
        /// Muestra la ventana de autenticación
        /// </summary>
        public void Mostar()
        {
            base.Show();
        }

        /// <summary>
        /// Muestra la ventana de autenticación
        /// </summary>
        public void Mostar(FormaSinComparacion forma )
        {
            miFormaSinComparacion = forma;
            base.Show();
        }

        public void Preguntar(string encabezado, string mensaje)
        {
            etiquetaEncabezado.Text = encabezado;
            etiquetaMensajeGeneral.Text =  mensaje;

            base.Show();
        }
        
        /// <summary>
        /// Carga la lista de usuarios a partir de su fábrica 
        /// </summary>
        private void CargarUsuarios()
        {
            usuarios = Fabricas.fabricaUsuarios.ObtenerExistentes();
        }

        /// <summary>
        /// Se realiza la validación del usuario
        /// </summary>
        private void botonAceptar_Click(object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
        {
            bool encontro = false;
            string strNewValue;
            etiquetaEncabezado.Text = String.Empty ;

            try
            {
                //// Create a request using a URL that can receive a post.   
                //WebRequest request = WebRequest.Create("http://192.168.0.138:82/servicios/sincronizacion.asmx/SetData");
                //// Set the Method property of the request to POST.  
                //request.Method = "POST";
                //// Create POST data and convert it to a byte array.  
                ////string postData = "This is a test that posts this string to a Web server.";
                ////byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                ////// Set the ContentType property of the WebRequest.  
                ////request.ContentType = "application/x-www-form-urlencoded";
                //// Set the ContentLength property of the WebRequest.  

                //strNewValue = "idReader=666&sentido=2&value=4";

                //byte[] byteArray = Encoding.UTF8.GetBytes(string.Format(strNewValue));
                //request.ContentLength = byteArray.Length;
                //// Get the request stream.  
                //Stream dataStream = request.GetRequestStream();
                //// Write the data to the request stream.  
                //dataStream.Write(byteArray, 0, byteArray.Length);
                //// Close the Stream object.  
                //dataStream.Close();
                //// Get the response.  
                //WebResponse response = request.GetResponse();
                //// Display the status.  
                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                //// Get the stream containing content returned by the server.  
                //dataStream = response.GetResponseStream();
                //// Open the stream using a StreamReader for easy access.  
                //StreamReader reader = new StreamReader(dataStream);
                //// Read the content.  
                //string responseFromServer = reader.ReadToEnd();
                //// Display the content.  
                //Console.WriteLine(responseFromServer);
                //// Clean up the streams.  
                //reader.Close();
                //dataStream.Close();
                //response.Close();
               
                
                //etiquetaMensajeGeneral.Text = HttpGet();

            }
            catch (Exception e)
            {
                etiquetaMensajeGeneral.Text = e.Message;
            }

            foreach (string usuario in usuarios)
            {
                
                if (usuario == contrasenaIngresada)
                {
                    miFormaSinComparacion.terminar();
                    //formaPrincipal.MostrarPantalla(usuario);
                    //encontro = true;
                    //cajaClave.Text = string.Empty;
                    //miFormaPrincipal = new FormaPrincipal("Principal", Globales.ColorAplicacion());
                    //miFormaPrincipal.MostrarPantalla(usuario);

                    //Close();
                    //Confirmacion();
                    break;
                }
            }
            if (!encontro)
            {
                etiquetaResultadoVerificacion.Text = "Clave de usuario no registrada";
            }
            
        }

        static System.Net.WebRequest req = System.Net.WebRequest.Create("http://192.168.0.126:8080/");
       

        public static string HttpGet()
        {
            
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            return sr.ReadToEnd().Trim();
        }


        /// <summary>
        /// Se cierra la ventana y regresa a la aplicación principal
        /// </summary>
        private void botonCancelar_Click(object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
        {
            cajaClave.Text = string.Empty;
            Close();
            //this.Close();
            //Cancelacion();
        }

        /// <summary>
        /// Método que se ejecuta cuando se presiona el teclado físico
        /// </summary>
        void SePresiontaTecla(Object o, Intermec.Printer.UI.Keypad.KeyEventArgs e)
        {
            if ((e.KeyChar.ToString() != "\t") && (e.KeyChar.ToString() != "1") && (e.KeyChar.ToString() != "2")
                && (e.KeyChar.ToString() != "3") && (e.KeyChar.ToString() != "4") && (e.KeyChar.ToString() != "5")
                && (e.KeyChar.ToString() != "6") && (e.KeyChar.ToString() != "7") && (e.KeyChar.ToString() != "8")
                && (e.KeyChar.ToString() != "9"))
            {
                return;
            }

            if (e.KeyChar.ToString() == "\t")
            {
                contrasenaIngresada = string.Empty;
                cajaClave.Text = string.Empty;
            }
            else
            {
                contrasenaIngresada += e.KeyChar.ToString();
            }

            for (int i = 0; i <= contrasenaIngresada.Length-1; i++)
            {
                cajaClave.Text += "*";
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            this.Close();
        }

        #endregion
    }
}
