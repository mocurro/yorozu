﻿using System;
using System.Collections.Generic;
using System.Text;
using AutomatizacionEtiquetado.UI.Base;
using Intermec.Printer;
using AutomatizacionEtiquetado.Comun.Globales;
using Automatizacion_de_etiquetado.Enumeradores;
using AutomatizacionEtiquetado.Datos.Clases;
using AutomatizacionEtiquetado.Datos.Fabricas;
using Intermec.ICPUIElements;
using AutomatizacionEtiquetado.Comun.Clases;
using System.Threading;
using AutomatizacionEtiquetado.Comun.Constantes;
using System.Globalization;
using System.Timers;

namespace AutomatizacionEtiquetado.UI.Aplicacion
{
   public class FormaSinComparacion : FormaBase, IDisposable
    {
      
      Button botonSalir;
      Button botonRegresar;
      string NoParte = string.Empty;
      String Linea= string.Empty;         
      TextLabel lblCantidad;
      TextLabel lblMensajeRespuestaImpresion;
      TextLabel lblEtiquetasImpresas;
      int cantidaImpresa;
      TextLabel lblMaquina;
      TextLabel lblTextoMaquina;
      string maquina = string.Empty;
      String Turno;
      String turnoAnterior="";
      int Consecutivo;

      //String NoParte;
      string[] turnos;

      NumeroParte numeroParteEnMemoria;

      String[] listaNumerosDeParte;

        // agregando el hilo para escuchar la impresion

      private System.Timers.Timer MonitorPulsos;
      Communication.IndustrialInterface industrialInterface = null;
      Thread TareaMonitoreoInterfaceIndustrial = null;
      System.ComponentModel.BackgroundWorker backgroundVerificarPuerto;
      private volatile bool _shouldStop = false;

      FabricaTurnos fabricaTurnos = new FabricaTurnos();

        

      /// <summary>
      /// Forma ulitzada para realizar la validación de la sesión de usuario
      /// </summary>
      FormaSesion miFormaSesion;


      public FormaSinComparacion(string nombreForma, Color color)
          : base(nombreForma, color, "")
        {

            ObtenerNumerosDeParte();
           //ObtenerNumerosDeParte2();
           
         ObtenerConfiguraciones();            

          lblMaquina = Controles.GenerarEtiqueta(this.Name, "lblMaquina", 10, 40, "Línea: ", 15, ICPUIClr.clrBlack);
          lblTextoMaquina = Controles.GenerarEtiqueta(this.Name, "lblTextoMaquina", 60, 40, maquina, 15, ICPUIClr.clrBlack);
          lblTextoMaquina.Text = NoParte;
          
          //lblCantidad = Controles.GenerarEtiqueta(this.Name, "lblCantidad", 10, 85, "No. Parte:");

            lblMensajeRespuestaImpresion = Controles.GenerarEtiqueta(this.Name, "lblMensajeRespuestaImpresion", 10, 120, "");
            lblEtiquetasImpresas = Controles.GenerarEtiqueta(this.Name, "lblEtiquetasImpresas", 10, 155, "", 8, ICPUIClr.clrBlack);       

           MotorImpresion.ErrorImpresion += new MotorImpresion.MotorImpresion_Error(MotorImpresion_ErrorImpresion);
           MotorImpresion.ImpresionExitosa += new MotorImpresion.MotorImpresion_Exitosa(MotorImpresion_ImpresionExitosa);          

            //botonSalir = Controles.GenerarBoton(this.Name, "botonSalir", 125, 280, Button.BtnStyle.RectHalf, "Reimprimir");
            botonRegresar = Controles.GenerarBoton(this.Name, "botonRegresar", 10, 100, Button.BtnStyle.RectHalf, "Salir");           
            //botonSalir.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(BotonSalir_Click);
            botonRegresar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(BotonRegresar_Click);

           this.MonitorPulsos = new System.Timers.Timer(250);
           this.MonitorPulsos.Interval = 250;
            numeroParteEnMemoria = new NumeroParte();


            //ProcesarImpresion2(true);
        }


      //public FormaSinComparacion(string nombreForma, Color color,string n)
      //    : base(nombreForma, color, "")
      //{

      //    ObtenerNumerosDeParte();
      //    ObtenerConfiguraciones();

      //    blMaquina = Controles.GenerarEtiqueta(this.Name, "lblMaquina", 5, 20, "Línea: ", 15, ICPUIClr.clrBlack);
      //    lblTextoMaquina = Controles.GenerarEtiqueta(this.Name, "lblTextoMaquina", 30, 20, "Hola", 15, ICPUIClr.clrBlack);
      //    lblCantidad = Controles.GenerarEtiqueta(this.Name, "lblCantidad", 5, 40, "No. Parte:");

      //    lblMensajeRespuestaImpresion = Controles.GenerarEtiqueta(this.Name, "lblMensajeRespuestaImpresion", 5, 60, "");
      //    lblEtiquetasImpresas = Controles.GenerarEtiqueta(this.Name, "lblEtiquetasImpresas", 5, 70, "");

      //    MotorImpresion.ErrorImpresion += new MotorImpresion.MotorImpresion_Error(MotorImpresion_ErrorImpresion);
      //    MotorImpresion.ImpresionExitosa += new MotorImpresion.MotorImpresion_Exitosa(MotorImpresion_ImpresionExitosa);

      //    botonSalir = Controles.GenerarBoton(this.Name, "botonSalir", 125, 280, Button.BtnStyle.RectHalf, "Reimprimir");
      //    botonRegresar = Controles.GenerarBoton(this.Name, "botonRegresar", 5, 140, Button.BtnStyle.RectHalf, "Salir");
      //    botonSalir.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(BotonSalir_Click);
      //    botonRegresar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(BotonRegresar_Click);

      //    this.MonitorPulsos = new System.Timers.Timer(500);
      //    this.MonitorPulsos.Interval = 500;
      //    numeroParteEnMemoria = new NumeroParte();



      //}



      private void ObtenerConfiguraciones()
      {
          //////Globales.RegistrarEvento("Obteniendo configuraciones", TiposMensajes.Aviso);
          //Configuracion configuracion = new Configuracion();
         // String[] configuraciones = Fabricas.fabricaConfiguracion.ObtenerConfiguraciones();
          //maquina = configuraciones[0].Split(',')[0];
          //configuracion.cantidadContenedor = int.Parse(configuraciones[0].Split(',')[1]);

          string linea = Fabricas.fabricaNumerosParte.ObtenerLinea();

          maquina = string.Format("{0}", linea);
          

      }

      public String LeerNumeroParte(String puerto)
      {       String error = "error";
                     
              
              if (listaNumerosDeParte.Length > 0)
              {

                  try
                  {
                      switch (puerto)
                      {
                          case "1":
                              if (listaNumerosDeParte[0].Length > 0)
                                  return listaNumerosDeParte[0];
                              else
                                  return error;
                          case "2":
                              if (listaNumerosDeParte[1].Length > 0)
                                  return listaNumerosDeParte[1];
                              else
                                  return error;
                          case "3":
                              if (listaNumerosDeParte[2].Length > 0)
                                  return listaNumerosDeParte[2];
                              else
                                  return error;
                          case "4":
                              if (listaNumerosDeParte[3].Length > 0)
                                  return listaNumerosDeParte[3];
                              else
                                  return error;
                          case "5":
                              if (listaNumerosDeParte[4].Length > 0)
                                  return listaNumerosDeParte[4];
                              else
                                  return error;
                          case "6":
                              if (listaNumerosDeParte[5].Length > 0)
                                  return listaNumerosDeParte[5];
                              else
                                  return error;
                          case "7":
                              if (listaNumerosDeParte[6].Length > 0)
                                  return listaNumerosDeParte[6];
                              else
                                  return error;
                          case "8":
                              if (listaNumerosDeParte[7].Length > 0)
                                  return listaNumerosDeParte[7];
                              else
                                  return error;
                          case "9":
                              if (listaNumerosDeParte[8].Length > 0)
                                  return listaNumerosDeParte[8];
                              else
                                  return error;
                          case "10":
                              if (listaNumerosDeParte[9].Length > 0)
                                  return listaNumerosDeParte[9];
                              else
                                  return error;
                          case "11":
                              if (listaNumerosDeParte[10].Length > 0)
                                  return listaNumerosDeParte[10];
                              else
                                  return error;
                          case "12":
                              if (listaNumerosDeParte[11].Length > 0)
                                  return listaNumerosDeParte[11];
                              else
                                  return error;
                          case "13":
                              if (listaNumerosDeParte[12].Length > 0)
                                  return listaNumerosDeParte[12];
                              else
                                  return error;
                          case "14":
                              if (listaNumerosDeParte[13].Length > 0)
                                  return listaNumerosDeParte[13];
                              else
                                  return error;
                          case "15":
                              if (listaNumerosDeParte[14].Length > 0)
                                  return listaNumerosDeParte[14];
                              else
                                  return error;

                      }
                  }
                  catch (Exception e)
                  {
                      return error;
                  }
              }

             
          

          return error;

      }


      private string ObtenerTurno(DateTime hora)
      {
          
          //if (turnos == null) {
              turnos = fabricaTurnos.ObtenerTurnos();
          //}
         
          Turno turnoEncontrado = null;
          //            TimeSpan horaActual = TimeSpan.Parse(string.Format("{0}:{1}", hora.Hour, hora.Minute));
          TimeSpan horaActual = new TimeSpan(0, hora.Hour, hora.Minute, 0, 0);
          try
          {
              string[] separador = new string[] { "|" };
              for (int i = 0; i < turnos.Length; i++)
              {
                  string valorTurno = turnos[i].Split(separador, StringSplitOptions.None)[2].ToString();
                  string iniciaTurno = turnos[i].Split(separador, StringSplitOptions.None)[0].ToString();
                  string finTurno = turnos[i].Split(separador, StringSplitOptions.None)[1].ToString();
                  Turno turnoArchivo = new Turno(valorTurno, iniciaTurno, finTurno);

                  if (turnoArchivo.inicio <= horaActual && turnoArchivo.fin >= horaActual)
                  {
                      turnoEncontrado = turnoArchivo;
                      break;
                  }
                  else
                  {
                      if (turnoArchivo.inicio > turnoArchivo.fin)
                      {
                          DateTime fechaInicia = new DateTime(hora.Year, hora.Month, hora.Day, turnoArchivo.inicio.Hours, turnoArchivo.inicio.Minutes, 0);
                          DateTime fechaFin = new DateTime(hora.Year, hora.Month, hora.AddDays(1).Day, turnoArchivo.fin.Hours, turnoArchivo.fin.Minutes, 0);
                          if ((hora >= fechaInicia) && (hora <= fechaFin))
                          {
                              turnoEncontrado = turnoArchivo;
                              break;
                          }
                          else
                          {
                              hora = hora.AddDays(1);
                              if ((hora >= fechaInicia) && (hora <= fechaFin))
                              {
                                  turnoEncontrado = turnoArchivo;
                                  break;
                              }
                          }
                      }
                  }

              }

              if (turnoEncontrado == null)
              {
                  //formaError.MostrarMensaje("Sin turno", "No se encontró el turno", TiposMensajes.Advertencia);
                  lblMaquina.ForeColor = ICPUIClr.clrRed;
                  lblMaquina.Text = "SIN TURNO";
              }
              else
              {
                  lblMaquina.ForeColor = ICPUIClr.clrGreen;
                  lblMaquina.Text = string.Empty;
                  return turnoEncontrado.valor;
              }
          }
          catch (Exception ex)
          {
              //formaError.MostrarMensaje("Error en la verificación del turno", ex.Message, TiposMensajes.ErrorInesperado);
              lblMaquina.ForeColor = ICPUIClr.clrRed;
              lblMaquina.Text = ex.Message.ToUpper();
              return "0";
          }
          return turnoEncontrado.valor;
      }


      private void ObtenerNumerosDeParte()
      {
          Globales.RegistrarEvento("Obteniendo numeros de parte", TiposMensajes.Aviso);
          listaNumerosDeParte = Fabricas.fabricaNumerosParte.ObtenerNumerosDeParte();
          if (listaNumerosDeParte.Length == 0)
              lblMensajeRespuestaImpresion.Text = "Sin números de parte";
      }

      private void ObtenerNumerosDeParte2()
      {
          //Globales.RegistrarEvento("Obteniendo numeros de parte", TiposMensajes.Aviso);
          NoParte = Fabricas.fabricaNumerosParte.ObtenerNumerosDeParte2();
          Consecutivo = Fabricas.fabricaNumerosParte.ObtenerConsecutivo();
          //if (listaNumerosDeParte.Length == 0)
          //    lblMensajeRespuestaImpresion.Text = "Sin números de parte";
      }

      void InicializarHilo() {
          try {
              //this.ProcesarImpresion("1");
              this.InicializarComunicacionIndustrial();
              this.MonitorPulsos.Start();
              this.MonitorPulsos.Elapsed += OnTimedEvent;
              this.backgroundVerificarPuerto = new System.ComponentModel.BackgroundWorker();
              Console.WriteLine("Attach dowork");
              this.backgroundVerificarPuerto.DoWork += backgroundVerificarPuerto_DoWork;
              Console.WriteLine("Attach runworkercompleted");
              this.backgroundVerificarPuerto.RunWorkerCompleted += this.backgroundVerificarPuerto_RunWorkerCompleted;
              Console.WriteLine("Tarea instanciada");
              //lblEtiquetasImpresas.Text = "Attach runworkercompleted";
          }catch(Exception ex){
              lblMensajeRespuestaImpresion.Text ="inicializacion" +ex.Message;  
          }
          

      }
      private void backgroundVerificarPuerto_RunWorkerCompleted(System.Object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
      {
          try
          {
              //rehabilitar el boton de reconectar
              _shouldStop = false;
              Console.WriteLine("Se termino la verificacion de pulso de impresión");
          }
          catch (Exception ex)
          {
              Console.WriteLine("Error al finalizar la tarea monitor pulso: " + ex.Message);
          }
      }
      private void backgroundVerificarPuerto_DoWork(System.Object sender, System.ComponentModel.DoWorkEventArgs e)
      {
          try
          {
              Console.WriteLine("Verificando puerto indistrial por tarea backgroud");
              //Iniciar el proceso de habilitar y deshabilitar
              _shouldStop = true;
              this.LeerPuertoIndustrial();
          }
          catch (Exception ex)
          {
              Console.WriteLine("Error al finalizar la tarea monitor pulso" + ex.Message);
          }
      }

      private void LeerPuertoIndustrial2()
      {
          Console.WriteLine("Obteniendo el numero depuerto industrial");
          int[] puertos = new int[1]; //MisConfiguraciones.PuertoIndustrial;
          //Se inicializan los numeros de puertos
          puertos[0] = 104;
          Boolean bandera104 = false;

          Console.WriteLine("Activando bandera Verificar pulso");
         
              if (ConsultarPuertoActivo2(104))
              {
                      bandera104 = true;

              }
              if (bandera104) {
                  this.ProcesarImpresion2(false);
              }

      }



      //lee el puerto industrial e idetifica que numero de parte que va a leer
      private void LeerPuertoIndustrial()
      {
          Console.WriteLine("Obteniendo el numero depuerto industrial");
          int[] puertos = new int[8]; //MisConfiguraciones.PuertoIndustrial;
          //Se inicializan los numeros de puertos
          puertos[0] = 104;
          puertos[0] = 101;
          puertos[1] = 102;
          puertos[2] = 103;
          puertos[3] = 104;
          puertos[4] = 105;
          puertos[5] = 106;
          puertos[6] = 107;
          puertos[7] = 108;
          Boolean bandera101 = false;
          Boolean bandera102 = false;
          Boolean bandera103 = false;
          Boolean bandera104 = false;

          Console.WriteLine("Activando bandera Verificar pulso");


          //Comentar
          for (int i = 0; i < puertos.Length; i++)
          {
              if (ConsultarPuertoActivo(puertos[i]))
              {
                  if (puertos[i] == 101)
                  {
                      bandera101 = true;
                  }
                  else if (puertos[i] == 102)
                  {

                      bandera102 = true;

                  }
                  else if (puertos[i] == 103)
                  {

                      bandera103 = true;

                  }
                  else if (puertos[i] == 104)
                  {

                      bandera104 = true;
                  }

              }
          }


          // combinacion bits: 1 0 0 0
          if (bandera101 & !bandera102 & !bandera103 & !bandera104)
          {
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("1");
              Console.WriteLine("Fin impresión OK");
          }
          else if (!bandera101 & bandera102 & !bandera103 & !bandera104)
          { // combinacion bits: 0 1 0 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("2");
              Console.WriteLine("Fin impresión OK");
          }
          else if (bandera101 & bandera102 & !bandera103 & !bandera104)
          { // combinacion bits: 1 1 0 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("3");
              Console.WriteLine("Fin impresión OK");
          }
          else if (!bandera101 & !bandera102 & bandera103 & !bandera104)
          { // combinacion bits: 0 0 1 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("4");
              Console.WriteLine("Fin impresión OK");
          }
          else if (bandera101 & !bandera102 & bandera103 & !bandera104)
          { // combinacion bits: 1 0 1 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("5");
              Console.WriteLine("Fin impresión OK");
          }
          else if (!bandera101 & bandera102 & bandera103 & !bandera104)
          { // combinacion bits: 0 1 1 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("6");
              Console.WriteLine("Fin impresión OK");
          }
          else if (bandera101 & bandera102 & bandera103 & !bandera104)
          { // combinacion bits: 1 1 1 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("7");
              Console.WriteLine("Fin impresión OK");
          }
          //Nuevos números de parte
          else if (!bandera101 & !bandera102 & !bandera103 & bandera104)
          { // combinacion bits: 0 0 0 1
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("8");
              Console.WriteLine("Fin impresión OK");
          }
          else if (bandera101 & !bandera102 & !bandera103 & bandera104)
          { // combinacion bits: 1 0 0 1
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("9");
              Console.WriteLine("Fin impresión OK");
          }
          else if (!bandera101 & bandera102 & !bandera103 & bandera104)
          { // combinacion bits: 0 1 0 1
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("10");
              Console.WriteLine("Fin impresión OK");
          }
          else if (bandera101 & bandera102 & !bandera103 & bandera104)
          { // combinacion bits: 1 1 0 1
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("11");
              Console.WriteLine("Fin impresión OK");
          }
          else if (!bandera101 & !bandera102 & bandera103 & bandera104)
          { // combinacion bits: 0 0 1 1
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("12");
              Console.WriteLine("Fin impresión OK");
          }
          else if (bandera101 & !bandera102 & bandera103 & bandera104)
          { // combinacion bits: 1 0 1 1
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("13");
              Console.WriteLine("Fin impresión OK");
          }
          else if (!bandera101 & bandera102 & bandera103 & bandera104)
          { // combinacion bits: 0 1 1 1
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("14");
              Console.WriteLine("Fin impresión OK");
          }
          else if (bandera101 & bandera102 & bandera103 & bandera104)
          { // combinacion bits: 1 1 1 1
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("15");
              Console.WriteLine("Fin impresión OK");
          }
          //Fin comentar
      }

      private bool ProcesarImpresion(String lineadeArray)
      {
        
          try {
              this.MonitorPulsos.Stop();
          
          string arrayNumeroParte = LeerNumeroParte(lineadeArray);

          if (!arrayNumeroParte.Equals("error"))
          {

              NumeroParte numeroParte = new NumeroParte(arrayNumeroParte, maquina, "");
                  numeroParte.linea = maquina;
                  numeroParteEnMemoria = numeroParte;
                  String[] imagenes = null;// Fabricas.fabricaNumerosParte.ObtenerImagenes();

                  cantidaImpresa = MotorImpresion.Imprimir(Constantes.RutaArchivoPlantilla, VariablesImpresion(numeroParte), true, 1, imagenes);
                  MotorImpresion_ErrorImpresion(numeroParte.codigo, null);

          }
          else
          {
              lblEtiquetasImpresas.Text = arrayNumeroParte;
          }
          this.MonitorPulsos.Start();
          
         
          }
          catch( Exception e){
              lblEtiquetasImpresas.Text = "477"+e.Message ;
          }
           
          return true;

      }


      private bool ProcesarImpresion2(bool esPrecarga)
      {

          try
          {

              if (!esPrecarga)
              {
                  this.MonitorPulsos.Stop();

                  string arrayNumeroParte = NoParte;
                  DateTime fecha = DateTime.Now;

                  Turno = ObtenerTurno(fecha);

                  string serial = "";

                  //3 día del año
                  //1 ultimo digito de año
                  //1 turno
                  //en total 13 dígitos
                  string diaAño = fecha.DayOfYear.ToString("D3");
                  string año = fecha.Year.ToString();
                  año = año.Substring(año.Length - 1, 1);
                  serial = string.Format("{0}{1}{2}{3}", diaAño, año, Turno,Consecutivo.ToString().PadLeft(8, '0'));                  
                  
                  Consecutivo += 1;
                  if (Consecutivo == 100000000)
                  {
                      Consecutivo = 1;
                  }

                  lblMaquina.Text = serial;
                  //string serialAimprimir = serial.PadRight(13, '0');

                  //lblMensajeRespuestaImpresion.Text = serialAimprimir;
                  NumeroParte numeroParte = new NumeroParte(arrayNumeroParte, serial);
                  numeroParte.linea = maquina;
                  numeroParteEnMemoria = numeroParte;
                  String[] imagenes = null;
                  //lblMaquina.Text = "Antes de imprimir";
                  //if (turnoAnterior.Equals(Turno))
                  //{
                  //    //lblMaquina.Text = "Antes de imprimir3";
                  //    cantidaImpresa = MotorImpresion.Imprimir3(Constantes.RutaArchivoPlantilla, VariablesImpresion(numeroParte), true, 1, true);
                      
                  //}
                  //else
                  //{
                      //lblMaquina.Text = "Antes de imprimir2";
                      cantidaImpresa = MotorImpresion.Imprimir3(Constantes.RutaArchivoPlantilla, VariablesImpresion(numeroParte), true, 1, false);
                  //}

                  turnoAnterior = Turno;
                  //lblMaquina.Text = fecha.ToString();

                  Fabricas.fabricaNumerosParte.guardarConsecutivo(Consecutivo);
                  this.MonitorPulsos.Start();
              }
              else {

                  string arrayNumeroParte = NoParte;
                  DateTime fecha = DateTime.Now;

                  Turno = ObtenerTurno(fecha);
                  string serial = "";

                  //3 día del año
                  //1 ultimo digito de año
                  //1 turno
                  //en total 13 dígitos
                  string diaAño = fecha.DayOfYear.ToString("D3");
                  string año = fecha.Year.ToString();
                  año = año.Substring(año.Length - 1, 1);
                  serial = string.Format("{0}{1}{2}{3}", diaAño, año, Turno,Consecutivo.ToString().PadLeft(8, '0'));
                  //Consecutivo += 1;
                  //string serialAimprimir = serial.PadRight(13, '0');

                  //lblMensajeRespuestaImpresion.Text = serialAimprimir;
                  NumeroParte numeroParte = new NumeroParte(arrayNumeroParte, serial);
                  numeroParte.linea = maquina;
                  numeroParteEnMemoria = numeroParte;
                  String[] imagenes = null;
                 
                      
                      cantidaImpresa = MotorImpresion.Imprimir4(Constantes.RutaArchivoPlantilla, VariablesImpresion(numeroParte));
            

                  turnoAnterior = Turno;
                  lblMaquina.Text = "OK";
                  //lblMaquina.Text = fecha.ToString();
              }
              


          }
          catch (Exception e)
          {
              lblEtiquetasImpresas.Text = e.Message;//.Substring(40, e.StackTrace.Length-1 );
          }

          return true;

      }

      private void Reimprimir() {

          if (numeroParteEnMemoria.codigo != null)
          {
              String[] imagenes=null; //= Fabricas.fabricaNumerosParte.ObtenerImagenes();
              cantidaImpresa = MotorImpresion.Imprimir(Constantes.RutaArchivoPlantilla, VariablesImpresion(numeroParteEnMemoria), true, 1, imagenes);
          }
      }
      private bool ConsultarPuertoActivo(int puerto)
      {
          bool readSignal;
          Console.WriteLine("Verificando señal en el puerto");
          readSignal = industrialInterface.GetPort(puerto);
          Console.WriteLine("La lectura del puerto industrial es: {0}", readSignal.ToString());
          if (readSignal == true)
          {
              Console.WriteLine("Se detecto pulso en el puerto: " + puerto);
              return true ;
             
          }
          else
          {
              Console.WriteLine("No se detecto señal en el puerto");
              return false;

          }
      }

      private bool ConsultarPuertoActivo2(int puerto)
      {
          bool readSignal;
          Console.WriteLine("Verificando señal en el puerto");
          readSignal = industrialInterface.GetPort(puerto);
          Console.WriteLine("La lectura del puerto industrial es: {0}", readSignal.ToString());
          if (readSignal == true)
          {
              Console.WriteLine("Se detecto pulso en el puerto: " + puerto);
              return false;

          }
          else
          {
              Console.WriteLine("No se detecto señal en el puerto");
              return true;

          }
      }

      private void OnTimedEvent(Object source, ElapsedEventArgs e)
      {
          Console.WriteLine("Tick timer");
          if (this.backgroundVerificarPuerto.IsBusy != true)
          {
              //Iniciar la tarea
              Console.WriteLine("Monitoreo iniciado");
              this.backgroundVerificarPuerto.RunWorkerAsync();
          }
          else
          {
              Console.WriteLine("Se esta realizando una tarea");
          }
                   
      }

      void InicializarComunicacionIndustrial()
      {
          Console.WriteLine("CommunicationIndustrialInterface");
          this.industrialInterface = new Communication.IndustrialInterface();
      }

        

      void MotorUSB_DatosRecibidos(string datos)
      {        

      }


      void MotorImpresion_ImpresionExitosa(bool incrementarConteo)
      {
      }

      void MotorImpresion_ErrorImpresion(string mensajeError, Drawing plantilla)
      {        
          lblMensajeRespuestaImpresion.Text = mensajeError;
          lblMensajeRespuestaImpresion.ForeColor = ICPUIClr.clrRed;
      }

      
      public void MostrarPantalla()
      {
          try
          {       Thread.Sleep(2000);
              InicializarHilo();             
              base.Show();
          }
          catch (Exception ex)
          {
              Globales.RegistrarEvento(ex.Message, TiposMensajes.ErrorInesperado);

              lblMensajeRespuestaImpresion.Text = "inicializa" +ex.Message;

          }
      }

      private void BotonSalir_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
      {        

          Reimprimir();
      }

      FormaSesion formaSesion
      {
          get
          {
              if (miFormaSesion == null)
              {
                  miFormaSesion = new FormaSesion(Guid.NewGuid().ToString() + " - Sesion", Globales.ColorAplicacion());
                  //miFormaSesion.Confirmacion += new FormaSesion.FormaSesion_Confirmacion();
                  //miFormaSesion.Cancelacion += new FormaSesion.FormaSesion_Cancelacion();
              }
              return miFormaSesion;
          }
          set
          {
              miFormaSesion = value;
          }
      }


      public void terminar() {
          MonitorPulsos.Stop();
          Forms.Shutdown();
      }


      private void BotonRegresar_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
      {
         
         

          try
          {
              Forms.Shutdown();
              //formaSesion.Mostar(this);
              //MonitorPulsos.Stop();
              //Forms.Shutdown();
          }
          catch (Exception ex)
          {
              lblMensajeRespuestaImpresion.Text =  ex.Message;
              //startupErr.Text = ex.ToString();
          }
          //previo
          //this.Close();

          
      }        



              #region IDisposable Members

              public void Dispose()
              {
                  this.Close();
              }

              #endregion

    }
}
