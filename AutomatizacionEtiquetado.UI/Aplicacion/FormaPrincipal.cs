﻿using System;
using System.Collections.Generic;
using System.Text;
using AutomatizacionEtiquetado.UI.Base;
using Intermec.Printer;
using AutomatizacionEtiquetado.Comun.Globales;
using Automatizacion_de_etiquetado.Enumeradores;
using AutomatizacionEtiquetado.Datos.Clases;
using AutomatizacionEtiquetado.Datos.Fabricas;
using Intermec.ICPUIElements;
using AutomatizacionEtiquetado.Comun.Clases;
using System.Threading;
using AutomatizacionEtiquetado.Comun.Constantes;
using System.Globalization;

namespace AutomatizacionEtiquetado.UI.Aplicacion
{
    /// <summary>
    /// Pantalla principal de la aplicación
    /// </summary>
    public class FormaPrincipal : FormaBase, IDisposable
    {

        /// <summary>
        /// Contiene la lista de números de parte
        /// </summary>
        String[] listaNumerosParte;

        /// <summary>
        /// Control para la lista desplegable de números de parte
        /// </summary>
        DataGrid listaDesplegable;

        /////// <summary>
        /////// Control para la caja de texto para la cantidad inicial
        /////// </summary>
        ////TextLabel cajaCantidadInicial;
       // TextBox cajaCantidadInicial;

        /// <summary>
        /// Control para la reimpresión
        /// </summary>
        ///Button botonReimprimir;

        /// <summary>
        /// Control para el cierre de la aplicación
        /// </summary>
        Button botonSalir;
        Button botonConfirmar;

        /// <summary>
        /// Control para identificar el número de parte actual
        /// </summary>
        TextLabel etiquetaNumeroDeParte;

        /// <summary>
        /// Control para identificar el número de piezas trabajadas
        /// </summary>
        //TextLabel etiquetaContador;

        ///////// <summary>
        ///////// Control para identificar el número de piezas requeridas
        ///////// </summary>
        //////TextLabel etiquetaCantidadRequerida;

         //<summary>
         //Control para identificar la leyenda de la cantidad inicial
         //</summary>
        //TextLabel lblCantidadInicial;

        TextLabel lblMaquina;

        TextLabel lblTextoMaquina;

        ///////// <summary>
        ///////// Contiene el conteo de piezas actuales
        ///////// </summary>
        //////int contador = 0;

        ///////// <summary>
        ///////// Contiene el número de piezas del estándar pack
        ///////// </summary>
        //////int piezasRequeridas = 0;

        /// <summary>
        /// Contiene el número de parte seleccionado por el usuario
        /// </summary>
        string numeroDeParteSeleccionado = string.Empty;

        /// <summary>
        /// Contiene el último número de parte que pasó la verificación
        /// </summary>
        string ultimoNumeroDeParte = string.Empty;

        /// <summary>
        /// Contiene si hubo algún cambio en el número de parte
        /// </summary>
        bool huboCambioEnCodigo = false;

        bool puedeCambiarNumeroParte = true;

        string maquina = string.Empty;

        string usuario = string.Empty;

        /// <summary>
        /// Contiene la última plantilla que fue enviada a la impresora
        /// </summary>
        Drawing ultimaPlantilla;

        /// <summary>
        /// Forma utilizada para un mensaje emergente
        /// </summary>
        FormaSesion miFormaPreguntaCambioNP;

        ///// <summary>
        ///// Forma utilizada para solicitar autorización para reimpresión
        ///// </summary>
        //FormaSesion miFormaAutorizaImpresion;

        ///////// <summary>
        ///////// Forma utilizada para un mensaje de error
        ///////// </summary>
        //////FormaMensaje miFormaError;

        /// <summary>
        /// Forma utilizada para realizar una pregunta al usuario
        /// </summary>
        FormaMensaje miFormaPregunta;

        /// <summary>
        /// Forma ulitzada para realizar la validación de la sesión de usuario
        /// </summary>
        FormaSesion miFormaSesion;

        ///////// <summary>
        ///////// Forma utilizada para la verificación de códigos
        ///////// </summary>
        //////FormaVerificacionCodigos miFormaVerificacion;

        /// <summary>
        /// Contiene si la aplicación puede continuar una vez que se muestra un mensaje
        /// </summary>
        bool puedeContinuar = true;

        ///////// <summary>
        ///////// Contiene la cantidad inicial con la que la aplicación inicia
        ///////// </summary>
        //////string cantidadInicial = "0";

        bool AceptarDatosSeriales = true;

        bool enEsperaDeMaster = false;

        

        FormaSesion formaPreguntaNP
        {
            get
            {
                if (miFormaPreguntaCambioNP == null)
                {
                    miFormaPreguntaCambioNP = new FormaSesion(Guid.NewGuid().ToString() + " - Sesion", Globales.ColorAplicacion());
                    miFormaPreguntaCambioNP.Confirmacion += new FormaSesion.FormaSesion_Confirmacion(CambiaNumeroParte);
                    miFormaPreguntaCambioNP.Cancelacion += new FormaSesion.FormaSesion_Cancelacion(Continuar);
                }
                return miFormaPreguntaCambioNP;
            }
            set
            {
                miFormaPreguntaCambioNP = value;
            }
        }

        //FormaSesion formaAutorizacionReimpresion
        //{
        //    get
        //    {
        //        if (miFormaAutorizaImpresion == null)
        //        {
        //            miFormaAutorizaImpresion = new FormaSesion(Guid.NewGuid().ToString() + " - Reimpresion", Globales.ColorAplicacion());
        //            miFormaAutorizaImpresion.Confirmacion += new FormaSesion.FormaSesion_Confirmacion(miFormaAutorizaImpresion_Confirmacion);
        //            miFormaAutorizaImpresion.Cancelacion += new FormaSesion.FormaSesion_Cancelacion(Continuar);
        //        }
        //        return miFormaAutorizaImpresion;
        //    }
        //    set
        //    {
        //        miFormaAutorizaImpresion = value;
        //    }
        //}


        //////FormaMensaje formaError
        //////{
        //////    get
        //////    {
        //////        if (miFormaError == null)
        //////        {
        //////            miFormaError = new FormaMensaje(Guid.NewGuid().ToString() + " - Error", Globales.ColorAplicacionError());
        //////            miFormaError.Confirmacion += new FormaMensaje.FormaMensaje_Confirmacion(mensajeEmergente_Confirmacion);
        //////        }
        //////        return miFormaError;
        //////    }
        //////    set
        //////    {
        //////        miFormaError = value;
        //////    }
        //////}

        //////FormaMensaje formaPregunta
        //////{
        //////    get
        //////    {
        //////        if (miFormaPregunta == null)
        //////        {
        //////            miFormaPregunta = new FormaMensaje(Guid.NewGuid().ToString() + " - Pregunta", Globales.ColorAplicacionError());
        //////            miFormaPregunta.Confirmacion += new FormaMensaje.FormaMensaje_Confirmacion(motorImpresion_Reimprimir);
        //////            miFormaPregunta.Cancelacion += new FormaMensaje.FormaMensaje_Cancelacion(mensajeEmergente_Confirmacion);
        //////        }
        //////        return miFormaPregunta;
        //////    }
        //////    set
        //////    {
        //////        miFormaPregunta = value;
        //////    }
        //////}

        FormaSesion formaSesion
        {
            get
            {
                if (miFormaSesion == null)
                {
                    miFormaSesion = new FormaSesion(Guid.NewGuid().ToString() + " - Sesion", Globales.ColorAplicacion());
                    miFormaSesion.Confirmacion += new FormaSesion.FormaSesion_Confirmacion(Salir);
                    miFormaSesion.Cancelacion += new FormaSesion.FormaSesion_Cancelacion(Continuar);
                }
                return miFormaSesion;
            }
            set
            {
                miFormaSesion = value;
            }
        }

        //////FormaVerificacionCodigos formaVerificacion
        //////{
        //////    get
        //////    {
        //////        if (miFormaVerificacion == null)
        //////        {
        //////            miFormaVerificacion = new FormaVerificacionCodigos(Guid.NewGuid().ToString() + " - Verificacion", Globales.ColorAplicacion());
        //////            miFormaVerificacion.Aprobado += new FormaVerificacionCodigos.VerificacionAprobada(verificacion_Aprobado);
        //////        }
        //////        return miFormaVerificacion;
        //////    }
        //////    set
        //////    {
        //////        miFormaVerificacion = value;
        //////    }
        //////}

        /// <summary>
        /// Constructor de la forma, carga todos los prerequisitos
        /// </summary>
        /// <param name="nombreForma">El nombre de la forma</param>
        /// <param name="color">El color de fondo</param>
        public FormaPrincipal(string nombreForma, Color color)
            : base(nombreForma, color, "")
        {
            ObtenerNumerosDeParte();
            ObtenerConfiguraciones();
            lblMaquina = Controles.GenerarEtiqueta(this.Name, "lblMaquina", 10, 40, "Línea: ", 15, ICPUIClr.clrBlack);
            lblTextoMaquina = Controles.GenerarEtiqueta(this.Name, "lblTextoMaquina", 60, 40, maquina , 15, ICPUIClr.clrBlack);
            //etiquetaContador = Controles.GenerarEtiqueta(this.Name, "lblContador", 120, 20, "Contador:      0");
            //etiquetaCantidadRequerida = Controles.GenerarEtiqueta(this.Name, "lblMaximo", 120, 37,string.Format("Requeridas: {0}",piezasRequeridas.ToString().PadRight(2)));
            //lblCantidadInicial = Controles.GenerarEtiqueta(this.Name, "lblInicial", 10, 50, "Cantidad :");
            //cajaCantidadInicial = Controles.GenerarCajaDeTexto(this.Name, "txtInicial", 95, 50, "1",50);
            //cajaCantidadInicial = Controles.GenerarEtiqueta(this.Name, "lblCantidadInicial", 150, 240, "0");

            this.KeyPress += new Intermec.Printer.UI.Keypad.KeyEventHandler(SePresiontaTecla);

            //etiquetaError = Controles.GenerarEtiquetaMultilinea(this.numeroDeParteSeleccionado, "lblError", 10, 85, 200, "Univers", 18, string.Empty);
            //etiquetaError.ForeColor = Globales.ColorAplicacionError();

            ///botonReimprimir = Controles.GenerarBoton(this.Name, "botonReimprimir", 10, 280,Button.BtnStyle.RectHalf, "Reimprimir");
            botonSalir = Controles.GenerarBoton(this.Name, "botonSalir", 10, 280, Button.BtnStyle.RectHalf, "Salir");
            botonConfirmar = Controles.GenerarBoton(this.Name, "botonConfirmar", 125, 280, Button.BtnStyle.RectHalf, "Confirmar");

            listaDesplegable.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(ListaNumerosParte_Click);
            botonConfirmar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(BotonConfirmar_Click);
            botonSalir.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(BotonSalir_Click);
            ///botonReimprimir.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(botonReimprimir_Click);
            MotorImpresion.ErrorImpresion += new MotorImpresion.MotorImpresion_Error(MotorImpresion_ErrorImpresion);
            MotorImpresion.ImpresionExitosa += new MotorImpresion.MotorImpresion_Exitosa(MotorImpresion_ImpresionExitosa);
            MotorSerial.MotorErrorInesperado += new MotorSerial.MotorSerial_ErrorInesperado(MotorSerial_MotorErrorInesperado);
            //MotorSerial.MotorRecibeDatos += new MotorSerial.MotorSerial_DatosRecibidos(MotorSerial_DatosRecibidos);
            MotorUSB.SinDispositivosEncontrados += new MotorUSB.MotorUSB_SinPuertosDisponibles(MotorUSB_SinPuertosEncontrados);
            
            //MotorUSB.DatosRecibidos += new MotorUSB.MotorUSB_DatosRecibidos(MotorUSB_DatosRecibidos);
            //try {
            //    servicio = new WebReference.Sincronizacion();
            //}
            //catch (Exception ex)
            //{
            //    etiquetaNumeroDeParte.Text = string.Format("{0}", ex.Message );
            //    etiquetaNumeroDeParte.ForeColor = ICPUIClr.clrRed;
            //}
            

        }

        void MotorUSB_DatosRecibidos(string datos)
        {
            bool encontrado = false;
            //if (datos.StartsWith("P"))
            //{
                //string nParte = datos.Substring(1, datos.Length - 1);
            string nParte = datos;
                foreach (string p in listaNumerosParte)
                    if (p == nParte)
                    {
                        encontrado = true;
                        break;
                    }
                     

                if (!encontrado)
                {
                    //lblMensajeError.ForeColor = ICPUIClr.clrRed;
                    //lblMensajeError.Text = "N.P. NO ENCONTRADO";
                    numeroDeParteSeleccionado = null;

                     etiquetaNumeroDeParte.Text = string.Format("{0}", "N.P. NO ENCONTRADO");
                    //etiquetaNumeroDeParte.Text=nParte;
                    etiquetaNumeroDeParte.ForeColor = ICPUIClr.clrRed;
                }
                else
                {
                    //lblMensajeError.ForeColor = ICPUIClr.clrRed;
                    //lblMensajeError.Text = string.Empty;
                    numeroDeParteSeleccionado = nParte;                    
                   // etiquetaNumeroDeParte.Text = string.Format("Actual: {0}", nParte);
                    etiquetaNumeroDeParte.ForeColor = ICPUIClr.clrBlack;

                    //NumeroParte numeroParte = new NumeroParte(nParte, maquina,usuario );
                    //ProcesarSalida(numeroParte);

                    etiquetaNumeroDeParte.Text = "";
                    FormaCantidadEtiquetas formaPrincipal = new FormaCantidadEtiquetas("Forma", Globales.ColorAplicacion(), maquina, numeroDeParteSeleccionado,listaNumerosParte);
                    formaPrincipal.MostrarPantalla();
                    

                }
            //}
            //else
            //{
            //    lblMensajeError.ForeColor = ICPUIClr.clrRed;
            //    lblMensajeError.Text = "N.P. INVALIDO";
            //}

            //if (!string.IsNullOrEmpty(numeroDeParteSeleccionado))
            //{
            //    if (datos.Substring(1, datos.Length - 1) != numeroDeParteSeleccionado)
            //    {
            //        lblMensajeError.ForeColor = ICPUIClr.clrRed;
            //        lblMensajeError.Text = "ETIQUETA INCORRECTA";
            //        puedeCambiarNumeroParte = false;
            //        AceptarDatosSeriales = false;
            //    }
            //    else
            //    {
            //        lblMensajeError.ForeColor = ICPUIClr.clrGreen;
            //        lblMensajeError.Text = datos;
            //        AceptarDatosSeriales = true;
            //        puedeCambiarNumeroParte = true;
            //    }
            //}
            
        }

        /// <summary>
        /// Método que se ejecuta cuando no hay un escáner conectado
        /// </summary>
        void MotorUSB_SinPuertosEncontrados()
        {
            puedeContinuar = false;
            //formaError.MostrarMensaje("No hay escáner", "Se requiere un escáner para la aplicación, conecte un escáner y reinicie la impresora", TiposMensajes.ErrorInesperado);

            etiquetaNumeroDeParte.Text = "SIN ESCÁNER";
            etiquetaNumeroDeParte.ForeColor = ICPUIClr.clrRed;

            //lblMensajeError.ForeColor = ICPUIClr.clrRed;
            //lblMensajeError.Text = "SIN ESCÁNER";
            
        }
              
        /// <summary>
        /// Método que se ejecuta cuando se presiona el teclado físico
        /// </summary>
        void SePresiontaTecla(Object o, Intermec.Printer.UI.Keypad.KeyEventArgs e)
        {
            if (e.KeyChar.ToString() =="\n")
            {
                if (enEsperaDeMaster)
                {
                    enEsperaDeMaster = false;
                }
                else
                {
                    enEsperaDeMaster = true;
                }
                ActualizarMensaje();
            }

            if (e.KeyChar.ToString() != "0") { 
            
            }

            //////else
            //////{
            //////    if (contador > 0) return;
            //////    if ((e.KeyChar.ToString() != "\t") && (e.KeyChar.ToString() != "1") && (e.KeyChar.ToString() != "2")
            //////        && (e.KeyChar.ToString() != "3") && (e.KeyChar.ToString() != "4") && (e.KeyChar.ToString() != "5")
            //////        && (e.KeyChar.ToString() != "6") && (e.KeyChar.ToString() != "7") && (e.KeyChar.ToString() != "8")
            //////        && (e.KeyChar.ToString() != "9"))
            //////    {
            //////        Console.WriteLine(e.KeyChar.ToString());
            //////        return;
            //////    }

            //////    try
            //////    {
            //////        if (e.KeyChar.ToString() == "\t")
            //////        {
            //////            cantidadInicial = "0";
            //////            cajaCantidadInicial.Text = "0";
            //////        }
            //////        else
            //////        {
            //////            if (cantidadInicial == "0")
            //////            {
            //////                cantidadInicial = e.KeyChar.ToString();
            //////            }
            //////            else
            //////            {
            //////                cantidadInicial += e.KeyChar.ToString();

            //////            }
            //////        }

            //////        if (int.Parse(cantidadInicial) >= piezasRequeridas)
            //////        {
            //////            cantidadInicial = (piezasRequeridas - 1).ToString();
            //////        }

            //////        cajaCantidadInicial.Text = cantidadInicial;
            //////        Refresh();
            //////    }
            //////    catch
            //////    {
            //////        cajaCantidadInicial.Text = "0";
            //////        cantidadInicial = "0";
            //////    }
            //////}
            
        }

        /// <summary>
        /// Muestra la pantalla principal
        /// </summary>
        public void MostrarPantalla()
        {
            try
            {
                base.Show();
                MotorUSB.IniciarEscaneo();
                MotorUSB.DatosRecibidos += new MotorUSB.MotorUSB_DatosRecibidos(MotorUSB_DatosRecibidos);
                Thread.Sleep(2000);
                MotorSerial.Iniciar();
            }
            catch (Exception ex)
            {
                Globales.RegistrarEvento(ex.Message, TiposMensajes.ErrorInesperado);
            }
        }

        /// <summary>
        /// Muestra la pantalla principal
        /// </summary>
        public void MostrarPantalla(string miUsuario)
        {
            try
            {
                base.Show();
                usuario = miUsuario;
                MotorUSB.IniciarEscaneo();
                MotorUSB.DatosRecibidos += new MotorUSB.MotorUSB_DatosRecibidos(MotorUSB_DatosRecibidos);
                Thread.Sleep(2000);
                MotorSerial.Iniciar();
            }
            catch (Exception ex)
            {
                Globales.RegistrarEvento(ex.Message, TiposMensajes.ErrorInesperado);
            }
        }
        

        /// <summary>
        /// Evento que se dispara cuando la impresión ocurrió de manera exitosa
        /// </summary>
        void MotorImpresion_ImpresionExitosa(bool incrementarConteo)
        {
            //lblMaquina.Text = string.Empty;
            //////if (incrementarConteo)
            //////{
            //////    Globales.RegistrarEvento(string.Format("Ultimo {0}, Banco {1}", ultimoNumeroDeParte, numeroDeParteSeleccionado), TiposMensajes.Aviso);
            //////    if (huboCambioEnCodigo)
            //////        contador = 0;
            //////    contador++;
            //////    //etiquetaContador.Text = string.Format("Contador:    {0}", (contador + int.Parse(cajaCantidadInicial.Text)).ToString().PadLeft(2));
            //////}
            //////this.Refresh();
            //////this.Refresh();
            
        }

        /// <summary>
        /// Evento que se levanta cuando existe un error de imrpesión
        /// </summary>
        /// <param name="mensajeError">El mensaje de error al imprimir</param>
        /// <param name="plantilla">La plantilla que fue enviada y produjo el error</param>
        void MotorImpresion_ErrorImpresion(string mensajeError,Drawing plantilla)
        {
            //MotorSerial.Detener();
            Thread.Sleep(100);
            ultimaPlantilla = plantilla;
            //formaPregunta.Preguntar("Error de impresión", string.Format("{0} ¿Reintentar?", mensajeError));
            //lblMensajeError.ForeColor = ICPUIClr.clrRed;
            //lblMensajeError.Text = mensajeError.ToUpper();

            etiquetaNumeroDeParte.Text = mensajeError;
            etiquetaNumeroDeParte.ForeColor = ICPUIClr.clrRed;
        }

        /// <summary>
        /// Se reintenta la impresión de la última plantilla
        /// </summary>
        void motorImpresion_Reimprimir()
        {
            MotorImpresion.Imprimir(ultimaPlantilla,true);
            MotorSerial.Reiniciar();
            LiberarForma();
        }

        /// <summary>
        /// Realiza la liberación de las formas de mensaje para la optimización de memoria
        /// </summary>
        void LiberarForma()
        {
            //if (miFormaVerificacion != null)
            //{
            //    miFormaVerificacion.Aprobado -= new FormaVerificacionCodigos.VerificacionAprobada(verificacion_Aprobado);
            //    formaVerificacion = null;
            //}

            //////if (miFormaPregunta != null)
            //////{
            //////    miFormaPregunta.Confirmacion -= new FormaMensaje.FormaMensaje_Confirmacion(motorImpresion_Reimprimir);
            //////    miFormaPregunta.Cancelacion -= new FormaMensaje.FormaMensaje_Cancelacion(mensajeEmergente_Confirmacion);
            //////    formaPregunta = null;
            //////}

            if (miFormaSesion != null)
            {
                miFormaSesion.Confirmacion -= new FormaSesion.FormaSesion_Confirmacion(Salir);
                miFormaSesion.Cancelacion -= new FormaSesion.FormaSesion_Cancelacion(Continuar);
                formaSesion = null;
            }

            //////if (miFormaError != null)
            //////{
            //////    miFormaError.Confirmacion -= new FormaMensaje.FormaMensaje_Confirmacion(mensajeEmergente_Confirmacion);
            //////    formaError = null;
            //////}

            if (miFormaPreguntaCambioNP != null)
            {
                miFormaPreguntaCambioNP.Confirmacion -= new FormaSesion.FormaSesion_Confirmacion(CambiaNumeroParte);
                miFormaPreguntaCambioNP.Cancelacion -= new FormaSesion.FormaSesion_Cancelacion(Continuar);
                formaPreguntaNP = null;
            }

            //if (miFormaAutorizaImpresion != null)
            //{
            //    miFormaAutorizaImpresion.Confirmacion -= new FormaSesion.FormaSesion_Confirmacion(miFormaAutorizaImpresion_Confirmacion);
            //    miFormaAutorizaImpresion.Cancelacion -= new FormaSesion.FormaSesion_Cancelacion(Continuar);
            //    formaAutorizacionReimpresion = null;
            //}
        }

        /// <summary>
        /// Obtiene la lista de números de parte
        /// </summary>
        private void ObtenerNumerosDeParte()
        {
            Globales.RegistrarEvento("Obteniendo numeros de parte", TiposMensajes.Aviso);

            listaNumerosParte = Fabricas.fabricaNumerosParte.ObtenerNumerosDeParte();
            listaDesplegable = Controles.GenerarListaDesplegable(this.Name, "cboNumeroParte", 10, 95, 225, 190, listaNumerosParte);

            etiquetaNumeroDeParte = Controles.GenerarEtiqueta(this.Name, "lblActual", 10, 70, " ");

            if (listaNumerosParte.Length == 0)
                etiquetaNumeroDeParte.Text = "Sin números de parte";
        }

        /// <summary>
        /// Obtiene las configuraciones generales
        /// </summary>
        private void ObtenerConfiguraciones()
        {
            //////Globales.RegistrarEvento("Obteniendo configuraciones", TiposMensajes.Aviso);
            Configuracion configuracion = new Configuracion();
            String[] configuraciones = Fabricas.fabricaConfiguracion.ObtenerConfiguraciones();
            //maquina = configuraciones[0].Split(',')[0];
            //configuracion.cantidadContenedor = int.Parse(configuraciones[0].Split(',')[1]);
            
            string linea = Fabricas.fabricaNumerosParte.ObtenerLinea();

            maquina = string.Format("{0}", linea);


        }

        /// <summary>
        /// Método que se ejecuta cuando se reciben datos del puerto
        /// </summary>
        /// <param name="datos">Los datos recibidos del puerto</param>
        private void MotorSerial_DatosRecibidos(string datos)
        {
            Globales.RegistrarEvento("Datos recibidos " + datos, TiposMensajes.Aviso);
            if (enEsperaDeMaster)
            {
                enEsperaDeMaster = false;
                ActualizarMensaje();
                return;
            }
            ActualizarMensaje();
            if (!AceptarDatosSeriales) return;
            if (string.IsNullOrEmpty(numeroDeParteSeleccionado)) return;
            //if (contador > 0) cajaCantidadInicial.Enabled = false;
            if (listaNumerosParte.Length > 0)
            {
                try
                {
                    NumeroParte numeroParte = new NumeroParte(datos);
                    if (numeroParte.codigo.Trim() == numeroDeParteSeleccionado)
                    {

                        if (string.IsNullOrEmpty(ultimoNumeroDeParte))
                        {
                            ultimoNumeroDeParte = numeroParte.codigo;
                            huboCambioEnCodigo = false;
                        }
                        else
                        {
                            if (ultimoNumeroDeParte != numeroParte.codigo)
                            {
                                huboCambioEnCodigo = true;
                                ultimoNumeroDeParte = numeroParte.codigo;
                            }
                            else
                            {
                                huboCambioEnCodigo = false;
                            }
                        }

                        Globales.RegistrarEvento("Ultimo: " + numeroParte.codigo, TiposMensajes.Aviso);
                        ProcesarSalida(numeroParte);
                    }
                    else //Inidcar que el número de parte recibido no corresponde al actual
                    {
                        //MotorSerial.Detener();
                        Globales.RegistrarEvento("Número de parte incorrecto", TiposMensajes.Advertencia);
                        puedeContinuar = true;
                        //formaError.MostrarMensaje("Número de parte incorrecto", "El número de parte del banco no corresponde con el seleccionado", TiposMensajes.Advertencia);

                        lblMaquina.ForeColor = ICPUIClr.clrRed;
                        lblMaquina.Text = "N.P INCORRECTO";
                        Refresh();
                    }
                }
                catch (Exception ex)
                {
                    Globales.RegistrarEvento("Error de interptretación de la cadena de banco " + ex.Message, TiposMensajes.ErrorInesperado);
                    MotorSerial.Detener();

                    //formaError.MostrarMensaje("Error en la interpretación",string.Format("Hubo un error al descifrar la cadena de texto recibida " +
                    //                                                         "desde el Banco: {0}",ex.Message), TiposMensajes.ErrorInesperado);
                    lblMaquina.ForeColor = ICPUIClr.clrRed;
                    lblMaquina.Text = ex.Message.ToUpper();
                }
            }
            else
            {
                return;
            }
        }

        

        /// <summary>
        /// Procesa la salida del proceso
        /// </summary>
        /// <param name="numeroParte">El número de parte a mostrar</param>
        private void ProcesarSalida(NumeroParte numeroParte)
        {
            //////int cantidadNueva = contador + 1;
            //////int cantidadComparacion = cantidadNueva + int.Parse(cantidadInicial);
           /* if (cajaCantidadInicial.Text.Equals("0") || cajaCantidadInicial .Text .Equals ("")) {
                etiquetaNumeroDeParte.Text = "Cantidad incorrecta.";
                etiquetaNumeroDeParte.ForeColor = ICPUIClr.clrRed;
            }
            else{
                if (int.Parse(cajaCantidadInicial.Text) > 0)
                {
                    etiquetaNumeroDeParte.ForeColor = ICPUIClr.clrBlack;
                    Refresh();
                    MotorImpresion.Imprimir(Constantes.RutaArchivoPlantilla, VariablesImpresion(numeroParte), true, int.Parse(cajaCantidadInicial.Text));
                }
                else {
                    etiquetaNumeroDeParte.Text = "Cantidad incorrecta.";
                    etiquetaNumeroDeParte.ForeColor = ICPUIClr.clrRed;
                }
                
            }*/

            //servicio.SetData("Prueba", 1, "Prueba");

            //////if (cantidadComparacion == piezasRequeridas) //Debe solicitar escaneo
            //////{
            //////    //MotorSerial.Detener();
            //////    //etiquetaContador.Text = string.Format("Contador:    {0}", "0".PadRight(2));
            //////    cajaCantidadInicial.Text = "0";
            //////    cantidadInicial = "0";
            //////    Thread.Sleep(500);
            //////    formaVerificacion.Verificar(numeroParte.codigo, piezasRequeridas);
            //////    AceptarDatosSeriales = false;
            //////}
        }

        /// <summary>
        /// Método que se ejecuta cuando el proceso de verificación del número de parte y cantidad fueron exitosos
        /// </summary>
        public void verificacion_Aprobado()
        {
            //////try
            //////{
            //////    //MotorSerial.Reiniciar();
            //////    AceptarDatosSeriales = true;
            //////    Thread.Sleep(250);
            //////    contador = 0;
            //////    //etiquetaContador.Text = string.Format("Contador:    {0}", contador.ToString().PadRight(2));
            //////    cajaCantidadInicial.Text = "0";
            //////    cantidadInicial = "0";
            //////    Refresh();
            //////    LiberarForma();
            //////    ManejadorMemoria.LiberarMemoria();
            //////}
            //////catch (Exception ex)
            //////{
            //////    Globales.RegistrarEvento(ex.Message, TiposMensajes.ErrorInesperado);
            //////}
            
        }

      
        /// <summary>
        /// Método que se ejecuta cuando ocurre un error en el motor de comunicación serial
        /// </summary>
        /// <param name="mensaje">El mensaje del error</param>
        private void MotorSerial_MotorErrorInesperado(string mensaje)
        {
            Globales.RegistrarEvento(mensaje, TiposMensajes.ErrorInesperado);
        }

        /// <summary>
        /// Método que reinicia el motor serial
        /// </summary>
        void mensajeEmergente_Confirmacion()
        {
            if (puedeContinuar)
                MotorSerial.Reiniciar();
            else
                MotorSerial.Detener();

            LiberarForma();
        }

        /// <summary>
        /// Método que reinicia el motor serial
        /// </summary>
        void Continuar()
        {
            MotorSerial.Reiniciar();
            LiberarForma();
        }


       

            private void BotonConfirmar_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
        {
            try
            {
                if (!puedeCambiarNumeroParte) return;
                if (listaDesplegable.SelectedItem != -1)
                {
                        numeroDeParteSeleccionado = listaDesplegable.SelectedText;
                        etiquetaNumeroDeParte.Text = numeroDeParteSeleccionado;
                        FormaCantidadEtiquetas formaPrincipal = new FormaCantidadEtiquetas("Forma", Globales.ColorAplicacion(), maquina, numeroDeParteSeleccionado,listaNumerosParte);
                        formaPrincipal.MostrarPantalla();
                                          
                     
                    
                }
            }
            catch (Exception ex)
            {
                Globales.RegistrarEvento(ex.Message, TiposMensajes.ErrorInesperado);
                etiquetaNumeroDeParte.Text = ex.Message;
            }
        }


        /// <summary>
        /// Se selecciona un número de parte de la lista
        /// </summary>
        private void ListaNumerosParte_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
        {
            /*try
            {
                if (!puedeCambiarNumeroParte) return;
                if (listaDesplegable.SelectedItem != -1)
                {
                        numeroDeParteSeleccionado = listaDesplegable.SelectedText;
                        FormaCantidadEtiquetas formaPrincipal = new FormaCantidadEtiquetas("Forma", Globales.ColorAplicacion(), maquina, numeroDeParteSeleccionado);
                        formaPrincipal.MostrarPantalla();
                        etiquetaNumeroDeParte.Text = "";                   
                     
                    
                }
            }
            catch (Exception ex)
            {
                Globales.RegistrarEvento(ex.Message, TiposMensajes.ErrorInesperado);
            }*/
        }

        /// <summary>
        /// Se cambia el número de parte a partir de un hilo
        /// </summary>
        void CambiaNumeroParte()
        {
            //////contador = 0;
            //////cajaCantidadInicial.Text = "0";
            //////cajaCantidadInicial.Enabled = true;
            etiquetaNumeroDeParte.Text = string.Format("Actual: {0}", listaDesplegable.SelectedText);
            //etiquetaContador.Text = string.Format("Contador:    {0}", "0".PadRight(2));
            numeroDeParteSeleccionado = listaDesplegable.SelectedText;
            etiquetaNumeroDeParte.ForeColor = ICPUIClr.clrBlack;

            LiberarForma();
            Refresh();
            MotorSerial.Reiniciar();
        }

        /// <summary>
        /// Se presiona el botón salir de la forma
        /// </summary>
        private void BotonSalir_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
        {
            /*
            FormaCantidadEtiquetas formaPrincipal = new FormaCantidadEtiquetas("Forma", Globales.ColorAplicacion(), maquina, listaDesplegable.SelectedText);
            formaPrincipal.MostrarPantalla();
            */
             
            //MotorSerial.Detener();

            //listaDesplegable.Click -= new Intermec.Printer.UI.Canvas.MouseEventHandler(ListaNumerosParte_Click);
            //botonSalir.Click -= new Intermec.Printer.UI.Canvas.MouseEventHandler(BotonSalir_Click);
           // MotorImpresion.ErrorImpresion -= new MotorImpresion.MotorImpresion_Error(MotorImpresion_ErrorImpresion);
           // MotorImpresion.ImpresionExitosa -= new MotorImpresion.MotorImpresion_Exitosa(MotorImpresion_ImpresionExitosa);
           // MotorSerial.MotorErrorInesperado -= new MotorSerial.MotorSerial_ErrorInesperado(MotorSerial_MotorErrorInesperado);
           // MotorUSB.SinDispositivosEncontrados -= new MotorUSB.MotorUSB_SinPuertosDisponibles(MotorUSB_SinPuertosEncontrados);
            //Previo
            //this.Close();
            try
            {
                formaSesion.Mostar();
                //Forms.Shutdown();
            }
            catch (Exception ex)
            {
                //startupErr.Text = ex.ToString();
            }
           //close();
            //formaSesion.Mostar();
        }

        ///////// <summary>
        ///////// Se presiona el botón reimprimir
        ///////// </summary>
        //////private void botonReimprimir_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
        //////{
        //////    if (string.IsNullOrEmpty(numeroDeParteSeleccionado)) return;
        //////    lblMensajeError.ForeColor = ICPUIClr.clrRed;
        //////    lblMensajeError.Text = string.Empty;
        //////    //formaAutorizacionReimpresion.Preguntar("Autorización requerida", "Ingrese la clave para autorizar la impresión");
        //////    NumeroParte numeroDeParte = new NumeroParte();
        //////    DateTime hora = DateTime.Now;
            
        //////    string semana =CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday).ToString().PadLeft(2,'0');
        //////    int dia = ObtenerDiaDeLaSemana(); ;//= (int) CultureInfo.InstalledUICulture.Calendar.GetDayOfWeek(DateTime.Now);
        //////    int turno = ObtenerTurno(hora);
        //////    if (turno > 0)
        //////    {
        //////        numeroDeParte.campo1 = "VW AG";
        //////        numeroDeParte.codigo = numeroDeParteSeleccionado;
        //////        numeroDeParte.hora = string.Format("{0}:{1}", hora.Hour.ToString().PadLeft(2, '0'), hora.Minute.ToString().PadLeft(2, '0'));
        //////        numeroDeParte.fecha = string.Format(" {0}  {1} {2}/{3}",turno, dia.ToString(), semana, DateTime.Now.Year.ToString().Substring(2, 2));
        //////        numeroDeParte.pais = "Mexico ZRP";
        //////        MotorImpresion.Imprimir(Constantes.RutaArchivoPlantilla, VariablesImpresion(numeroDeParte), false);
        //////    }
            
        //////    LiberarForma();
        //////}

        private string ObtenerTurno(DateTime hora)
        {
            FabricaTurnos fabricaTurnos = new FabricaTurnos();
            string[] turnos = fabricaTurnos.ObtenerTurnos();
            Turno turnoEncontrado = null;
//            TimeSpan horaActual = TimeSpan.Parse(string.Format("{0}:{1}", hora.Hour, hora.Minute));
            TimeSpan horaActual = new TimeSpan(0, hora.Hour, hora.Minute, 0, 0);
            try
            {
                string[] separador = new string[] { "|" };
                for (int i = 0; i < turnos.Length ; i++)
                {
                    string valorTurno = turnos[i].Split(separador, StringSplitOptions.None)[2].ToString();
                    string iniciaTurno = turnos[i].Split(separador, StringSplitOptions.None)[0].ToString();
                    string finTurno = turnos[i].Split(separador, StringSplitOptions.None)[1].ToString();
                    Turno turnoArchivo = new Turno(valorTurno, iniciaTurno, finTurno);

                    if (turnoArchivo.inicio <= horaActual && turnoArchivo.fin >= horaActual)
                    {
                        turnoEncontrado = turnoArchivo;
                        break;
                    }
                    else
                    {
                        if (turnoArchivo.inicio > turnoArchivo.fin)
                        {
                            DateTime fechaInicia = new DateTime(hora.Year, hora.Month, hora.Day, turnoArchivo.inicio.Hours, turnoArchivo.inicio.Minutes, 0);
                            DateTime fechaFin = new DateTime(hora.Year, hora.Month, hora.AddDays(1).Day, turnoArchivo.fin.Hours, turnoArchivo.fin.Minutes, 0);
                            if ((hora >= fechaInicia) && (hora <= fechaFin))
                            {
                                turnoEncontrado = turnoArchivo;
                                break;
                            }
                            else
                            {
                                hora = hora.AddDays(1);
                                if ((hora >= fechaInicia) && (hora <= fechaFin))
                                {
                                    turnoEncontrado = turnoArchivo;
                                    break;
                                }
                            }
                        }
                    }
                     
                }

                if (turnoEncontrado == null)
                {
                    //formaError.MostrarMensaje("Sin turno", "No se encontró el turno", TiposMensajes.Advertencia);
                    lblMaquina.ForeColor = ICPUIClr.clrRed;
                    lblMaquina.Text = "SIN TURNO";
                }
                else
                {
                    lblMaquina.ForeColor = ICPUIClr.clrGreen;
                    lblMaquina.Text = string.Empty;
                    return turnoEncontrado.valor;
                }
            }
            catch (Exception ex)
            {
                //formaError.MostrarMensaje("Error en la verificación del turno", ex.Message, TiposMensajes.ErrorInesperado);
                lblMaquina.ForeColor = ICPUIClr.clrRed;
                lblMaquina.Text = ex.Message.ToUpper();
                return "0";
            }
            return turnoEncontrado.valor;
        }

        /// <summary>
        /// Realiza la consulta del día de la semana
        /// </summary>
        /// <returns>El valor entero del día de la semana</returns>
        /// <remarks>Forza el día lunes como 1 independientemente de la configuración del equipo</remarks>
        private int ObtenerDiaDeLaSemana()
        {
            switch(CultureInfo.InstalledUICulture.Calendar.GetDayOfWeek(DateTime.Now))
            {
                case DayOfWeek.Monday:
                    return 1;
                case DayOfWeek.Tuesday:
                    return 2;
                case DayOfWeek.Wednesday:
                    return 3;
                case DayOfWeek.Thursday:
                    return  4;
                case DayOfWeek.Friday:
                    return 5;
                case DayOfWeek.Saturday:
                    return 6;
                case DayOfWeek.Sunday:
                    return 7;
                default:
                    return 0;
            }
            return 0;
        }

        

        /// <summary>
        /// Se cierra la aplicación
        /// </summary>
        private void Salir()
        {
            MotorSerial.Detener();
            Thread.Sleep(1000);
            Forms.Shutdown();
        }

        private void ActualizarMensaje()
        {
            if (enEsperaDeMaster)
            {
                etiquetaNumeroDeParte.Text = "Esperando master";
            }
            else
            {
                etiquetaNumeroDeParte.Text = string.Format("Actual: {0}", numeroDeParteSeleccionado);
                if (listaDesplegable.SelectedItem != -1)
                    etiquetaNumeroDeParte.Text = string.Format("Actual: {0}", numeroDeParteSeleccionado);
                else
                    etiquetaNumeroDeParte.Text = "";
            }
        }
        #region IDisposable Members

        public void Dispose()
        {
            this.Close();
        }

        #endregion
    }
}
