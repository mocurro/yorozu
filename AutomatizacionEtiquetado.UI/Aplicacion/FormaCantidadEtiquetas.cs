﻿using System;
using System.Collections.Generic;
using System.Text;
using AutomatizacionEtiquetado.UI.Base;
using Intermec.Printer;
using AutomatizacionEtiquetado.Comun.Globales;
using Automatizacion_de_etiquetado.Enumeradores;
using AutomatizacionEtiquetado.Datos.Clases;
using AutomatizacionEtiquetado.Datos.Fabricas;
using Intermec.ICPUIElements;
using AutomatizacionEtiquetado.Comun.Clases;
using System.Threading;
using AutomatizacionEtiquetado.Comun.Constantes;
using System.Globalization;
using System.Timers;

namespace AutomatizacionEtiquetado.UI.Aplicacion
{
    public class FormaCantidadEtiquetas : FormaBase, IDisposable
    {
      
      Button botonSalir;
      Button botonRegresar;
      string NoParte = string.Empty;
      String Linea= string.Empty;
      TextLabel lblCantidadInicial;
      TextLabel lblNoParte;
      TextLabel lblCantidad;
      TextLabel lblMensajeRespuestaImpresion;
      TextLabel lblEtiquetasImpresas;
      int cantidaImpresa;

      NumeroParte numeroParteEnMemoria;

      String[] listaNumerosDeParte;

        // agregando el hilo para escuchar la impresion

      private System.Timers.Timer MonitorPulsos;
      Communication.IndustrialInterface industrialInterface = null;
      Thread TareaMonitoreoInterfaceIndustrial = null;
      System.ComponentModel.BackgroundWorker backgroundVerificarPuerto;
      private volatile bool _shouldStop = false;
        
        
      public FormaCantidadEtiquetas(string nombreForma, Color color, String linea, String numeroParte, String[] numerosDeParte)
          : base(nombreForma, color, "")
        {
            this.listaNumerosDeParte = numerosDeParte;
           
          this.NoParte= numeroParte;
          this.Linea= linea;           

          lblNoParte = Controles.GenerarEtiqueta(this.Name, "lblNoParte", 10, 50, "No. Parte:");
          lblCantidadInicial = Controles.GenerarEtiqueta(this.Name, "lblCantidadInicial", 110, 50, "noParte:");
          lblCantidad = Controles.GenerarEtiqueta(this.Name, "lblCantidad", 10, 85, "No. Parte:");

          lblMensajeRespuestaImpresion = Controles.GenerarEtiqueta(this.Name, "lblMensajeRespuestaImpresion", 10, 120, "");
          lblEtiquetasImpresas = Controles.GenerarEtiqueta(this.Name, "lblEtiquetasImpresas", 10, 155, "");

            

           MotorImpresion.ErrorImpresion += new MotorImpresion.MotorImpresion_Error(MotorImpresion_ErrorImpresion);
           MotorImpresion.ImpresionExitosa += new MotorImpresion.MotorImpresion_Exitosa(MotorImpresion_ImpresionExitosa);          

            botonSalir = Controles.GenerarBoton(this.Name, "botonSalir", 125, 280, Button.BtnStyle.RectHalf, "Reimprimir");
            botonRegresar = Controles.GenerarBoton(this.Name, "botonRegresar", 10, 280, Button.BtnStyle.RectHalf, "Regresar");
           // botonRegresar = Controles.GenerarBoton(this.Name, "btnCancelar", 125, 280, Button.BtnStyle.RectHalf, "Cancelar");

            //listaDesplegable.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(ListaNumerosParte_Click);
            botonSalir.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(BotonSalir_Click);
            botonRegresar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(BotonRegresar_Click);

            ///botonReimprimir.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(botonReimprimir_Click);
            //MotorImpresion.ErrorImpresion += new MotorImpresion.MotorImpresion_Error(MotorImpresion_ErrorImpresion);
            //MotorImpresion.ImpresionExitosa += new MotorImpresion.MotorImpresion_Exitosa(MotorImpresion_ImpresionExitosa);
            //MotorSerial.MotorErrorInesperado += new MotorSerial.MotorSerial_ErrorInesperado(MotorSerial_MotorErrorInesperado);
            //MotorSerial.MotorRecibeDatos += new MotorSerial.MotorSerial_DatosRecibidos(MotorSerial_DatosRecibidos);
            //MotorUSB.SinDispositivosEncontrados += new MotorUSB.MotorUSB_SinPuertosDisponibles(MotorUSB_SinPuertosEncontrados);
            
            //MotorUSB.DatosRecibidos += new MotorUSB.MotorUSB_DatosRecibidos(MotorUSB_DatosRecibidos);
            //try {
            //    servicio = new WebReference.Sincronizacion();
            //}
            //catch (Exception ex)
            //{
            //    etiquetaNumeroDeParte.Text = string.Format("{0}", ex.Message );
            //    etiquetaNumeroDeParte.ForeColor = ICPUIClr.clrRed;
            //}
            this.MonitorPulsos = new System.Timers.Timer(500);
            this.MonitorPulsos.Interval = 500;

            numeroParteEnMemoria = new NumeroParte();

            CargarNp();
           // InicializarHilo();

        }

      public String LeerNumeroParte(String puerto)
      {       String error = "error";
                     
              
              if (listaNumerosDeParte.Length > 0)
              {

                  try
                  {
                      switch (puerto)
                      {
                          case "1":
                              if (listaNumerosDeParte[0].Length > 0)
                                  return listaNumerosDeParte[0];
                              else
                                  return error;
                          case "2":
                              if (listaNumerosDeParte[1].Length > 0)
                                  return listaNumerosDeParte[1];
                              else
                                  return error;
                          case "3":
                              if (listaNumerosDeParte[2].Length > 0)
                                  return listaNumerosDeParte[2];
                              else
                                  return error;
                          case "4":
                              if (listaNumerosDeParte[3].Length > 0)
                                  return listaNumerosDeParte[3];
                              else
                                  return error;
                          case "5":
                              if (listaNumerosDeParte[4].Length > 0)
                                  return listaNumerosDeParte[4];
                              else
                                  return error;
                          case "6":
                              if (listaNumerosDeParte[5].Length > 0)
                                  return listaNumerosDeParte[5];
                              else
                                  return error;
                          case "7":
                              if (listaNumerosDeParte[6].Length > 0)
                                  return listaNumerosDeParte[6];
                              else
                                  return error;
                          case "8":
                              if (listaNumerosDeParte[7].Length > 0)
                                  return listaNumerosDeParte[7];
                              else
                                  return error;

                      }
                  }
                  catch (Exception e)
                  {
                      return error;
                  }
              }

             
          

          return error;

      }


      void InicializarHilo() {
          try {
              this.InicializarComunicacionIndustrial();
              this.MonitorPulsos.Start();
              this.MonitorPulsos.Elapsed += OnTimedEvent;
              this.backgroundVerificarPuerto = new System.ComponentModel.BackgroundWorker();
              Console.WriteLine("Attach dowork");
              this.backgroundVerificarPuerto.DoWork += backgroundVerificarPuerto_DoWork;
              Console.WriteLine("Attach runworkercompleted");
              this.backgroundVerificarPuerto.RunWorkerCompleted += this.backgroundVerificarPuerto_RunWorkerCompleted;
              Console.WriteLine("Tarea instanciada");
          }catch(Exception ex){
              lblMensajeRespuestaImpresion.Text ="inicializacion" +ex.Message;  
          }
          

      }
      private void backgroundVerificarPuerto_RunWorkerCompleted(System.Object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
      {
          try
          {
              //rehabilitar el boton de reconectar
              _shouldStop = false;
              Console.WriteLine("Se termino la verificacion de pulso de impresión");
          }
          catch (Exception ex)
          {
              Console.WriteLine("Error al finalizar la tarea monitor pulso: " + ex.Message);
          }
      }
      private void backgroundVerificarPuerto_DoWork(System.Object sender, System.ComponentModel.DoWorkEventArgs e)
      {
          try
          {
              Console.WriteLine("Verificando puerto indistrial por tarea backgroud");
              //Iniciar el proceso de habilitar y deshabilitar
              _shouldStop = true;
              this.LeerPuertoIndustrial();
          }
          catch (Exception ex)
          {
              Console.WriteLine("Error al finalizar la tarea monitor pulso" + ex.Message);
          }
      }

      //lee el puerto industrial e idetifica que numero de parte que va a leer
      private void LeerPuertoIndustrial()
      {
          Console.WriteLine("Obteniendo el numero depuerto industrial");
          int[] puertos = new int[8]; //MisConfiguraciones.PuertoIndustrial;
          //Se inicializan los numeros de puertos
          puertos[0] = 101;
          puertos[1] = 102;
          puertos[2] = 103;
          puertos[3] = 104;
          puertos[4] = 105;
          puertos[5] = 106;
          puertos[6] = 107;
          puertos[7] = 108;
          Boolean bandera101 = false;
          Boolean bandera102 = false;
          Boolean bandera103 = false;
          Boolean bandera104 = false;

          Console.WriteLine("Activando bandera Verificar pulso");
          for (int i = 0; i < puertos.Length; i++)
          {
              if (ConsultarPuertoActivo(puertos[i]))
              {
                  if (puertos[i] == 101)
                  {
                      bandera101 = true;
                  }
                  else if (puertos[i] == 102)
                  {

                      bandera102 = true;

                  }
                  else if (puertos[i] == 103)
                  {

                      bandera103 = true;

                  }
                  else if (puertos[i] == 104)
                  {

                      bandera104 = true;
                  }

              }
          }


          // combinacion bits: 1 0 0 0
          if (bandera101 & !bandera102 & !bandera103 & !bandera104)
          {
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("1");
              Console.WriteLine("Fin impresión OK");
          }
          else if (!bandera101 & bandera102 & !bandera103 & !bandera104)
          { // combinacion bits: 0 1 0 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("2");
              Console.WriteLine("Fin impresión OK");
          }
          else if (bandera101 & bandera102 & !bandera103 & !bandera104)
          { // combinacion bits: 1 1 0 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("3");
              Console.WriteLine("Fin impresión OK");
          }
          else if (!bandera101 & !bandera102 & bandera103 & !bandera104)
          { // combinacion bits: 0 0 1 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("4");
              Console.WriteLine("Fin impresión OK");
          }
          else if (bandera101 & !bandera102 & bandera103 & !bandera104)
          { // combinacion bits: 1 0 1 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("5");
              Console.WriteLine("Fin impresión OK");
          }
          else if (!bandera101 & bandera102 & bandera103 & !bandera104)
          { // combinacion bits: 0 1 1 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("6");
              Console.WriteLine("Fin impresión OK");
          }
          else if (bandera101 & bandera102 & bandera103 & !bandera104)
          { // combinacion bits: 1 1 1 0
              Console.WriteLine("Se detecto pulso, Imprimiendo");
              //Imprimir();
              this.ProcesarImpresion("7");
              Console.WriteLine("Fin impresión OK");
          }
      }

      private bool ProcesarImpresion(String lineadeArray)
      {
        
          try {
              this.MonitorPulsos.Stop();
          
          string arrayNumeroParte = LeerNumeroParte(lineadeArray);

          if (!arrayNumeroParte.Equals("error"))
          {

              //lblMensajeRespuestaImpresion.Text = lineadeArray + "NP :" + arrayNumeroParte;
             // cajaCantidadInicial.Text = string.Format("{0},",arrayNumeroParte.ToString());            
              

              bool result = NoParte.Equals(arrayNumeroParte);
              //lblMensajeRespuestaImpresion.Text = result.ToString();

              if (NoParte.Equals(arrayNumeroParte))
              {


                  NumeroParte numeroParte = new NumeroParte(arrayNumeroParte, Linea, "");
                  numeroParteEnMemoria = numeroParte;
                  String[] imagenes = Fabricas.fabricaNumerosParte.ObtenerImagenes();
                  cantidaImpresa = MotorImpresion.Imprimir(Constantes.RutaArchivoPlantilla, VariablesImpresion(numeroParte), true, 1, imagenes);                  

                  
              }
              else {
                  Thread.Sleep(2000);
                  
              }
                   
                  
              

          }
          else
          {
              
          }
          this.MonitorPulsos.Start();
          
         
          }
          catch( Exception e){
              
          }
           
          return true;

      }

      private void Reimprimir() {

          if (numeroParteEnMemoria.codigo != null)
          {
              String[] imagenes = Fabricas.fabricaNumerosParte.ObtenerImagenes();
              cantidaImpresa = MotorImpresion.Imprimir(Constantes.RutaArchivoPlantilla, VariablesImpresion(numeroParteEnMemoria), true, 1, imagenes);
          }
      }
      private bool ConsultarPuertoActivo(int puerto)
      {
          bool readSignal;
          Console.WriteLine("Verificando señal en el puerto");
          readSignal = industrialInterface.GetPort(puerto);
          Console.WriteLine("La lectura del puerto industrial es: {0}", readSignal.ToString());
          if (readSignal == true)
          {
              Console.WriteLine("Se detecto pulso en el puerto: " + puerto);
              return true;
             
          }
          else
          {
              Console.WriteLine("No se detecto señal en el puerto");
              return false;

          }
      }

      private void OnTimedEvent(Object source, ElapsedEventArgs e)
      {
          Console.WriteLine("Tick timer");
          if (this.backgroundVerificarPuerto.IsBusy != true)
          {
              //Iniciar la tarea
              Console.WriteLine("Monitoreo iniciado");
              this.backgroundVerificarPuerto.RunWorkerAsync();
          }
          else
          {
              Console.WriteLine("Se esta realizando una tarea");
          }
                   
      }

      void InicializarComunicacionIndustrial()
      {
          Console.WriteLine("CommunicationIndustrialInterface");
          this.industrialInterface = new Communication.IndustrialInterface();
      }

        

      void MotorUSB_DatosRecibidos(string datos)
      {        

      }


      void MotorImpresion_ImpresionExitosa(bool incrementarConteo)
      {
      }

      void MotorImpresion_ErrorImpresion(string mensajeError, Drawing plantilla)
      {        
          lblMensajeRespuestaImpresion.Text = mensajeError;
          lblMensajeRespuestaImpresion.ForeColor = ICPUIClr.clrRed;
      }

        public void CargarNp(){
        lblCantidadInicial.Text= NoParte;        
        }
      public void MostrarPantalla()
      {
          try
          {       Thread.Sleep(2000);
              InicializarHilo();             
              base.Show();
          }
          catch (Exception ex)
          {
              Globales.RegistrarEvento(ex.Message, TiposMensajes.ErrorInesperado);

              lblMensajeRespuestaImpresion.Text = "inicializa" +ex.Message;

          }
      }

      private void BotonSalir_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
      {        

          Reimprimir();
      }





      private void BotonRegresar_Click(Object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
      {
                 

          lblMensajeRespuestaImpresion.Text = "";
          MonitorPulsos.Stop();          
          this.Close();

          
      }        



              #region IDisposable Members

              public void Dispose()
              {
                  this.Close();
              }

              #endregion

    }
}
