﻿using System;
using System.Collections.Generic;
using System.Text;
using AutomatizacionEtiquetado.UI.Base;
using Intermec.Printer;
using Intermec.ICPUIElements;
using AutomatizacionEtiquetado.Comun.Globales;

namespace AutomatizacionEtiquetado.UI.Aplicacion
{
    class FormaVerificacionCodigos: Form, IDisposable
    {

        public delegate void VerificacionAprobada();

        public event VerificacionAprobada Aprobado;


        /// <summary>
        /// Botón aceptar
        /// </summary>
        Button botonAceptar;

        /// <summary>
        /// Control de encabezado
        /// </summary>
        TextLabel etiquetaEncabezado;

        /// <summary>
        /// Control para identificar la etiqueta del mensaje
        /// </summary>
        TextMultlineLabel etiquetaMensajeGeneral;

        /// <summary>
        /// Control para identificar la etiqueta para el número de parte
        /// </summary>
        TextLabel etiquetaNumeroParte;

        /// <summary>
        /// Control para identificar la etiqueta para la cantidad
        /// </summary>
        TextLabel etiquetaCantidad;

        /// <summary>
        /// Control para identificar el resultado de la verificación
        /// </summary>
        TextMultlineLabel etiquetaResultadoVerificacion;

        /// <summary>
        /// Control para identificar la caja de texto del número de parte
        /// </summary>
        TextBox textBoxNumeroParte;

        /// <summary>
        /// Control para identificar la caja de texto para la cantidad
        /// </summary>
        TextBox textBoxCantidad;

        /// <summary>
        /// Contiene el número de parte esperado para la verificación
        /// </summary>
        string numeroDeParteEsperado;

        /// <summary>
        /// Contiene la cantidad esperada para la verificación
        /// </summary>
        int cantidadEsperada;

        /// <summary>
        /// Contiene si la verificación fue aprobada
        /// </summary>
        bool verificacionAprobada = false;

        bool numeroDeParteAprobado = false;

        bool cantidadAprobada = false;


        public FormaVerificacionCodigos(string nombreForma, Color color)
            :base(nombreForma,color)
        {
            etiquetaEncabezado = Controles.GenerarEtiqueta(this.Name, "lblEncabezado", 5, 5, "Verificación Kanban", 18, ICPUIClr.clrBlack);
            etiquetaMensajeGeneral = Controles.GenerarEtiquetaMultilinea(this.Name, "lblDescripcion", 10, 30,150,"Univers",16, "Escanee el número de parte y la cantidad de la etiqueta Kanban");
            
            etiquetaResultadoVerificacion = Controles.GenerarEtiquetaMultilinea(this.Name, "lblResultado", 5,220, 150,"Univers Bold",16, string.Empty);
            etiquetaResultadoVerificacion.ForeColor = Globales.ColorAplicacionError();

            etiquetaNumeroParte = Controles.GenerarEtiqueta(this.Name, "lblNumeroParte", 20, 100, "Número de parte");
            textBoxNumeroParte = Controles.GenerarCajaDeTexto(this.Name, "txtNumeroParte", 20, 130);
            textBoxNumeroParte.Enabled = false;
            
            etiquetaCantidad = Controles.GenerarEtiqueta(this.Name, "lblCantidad", 20, 160, "Cantidad");          
            textBoxCantidad = Controles.GenerarCajaDeTexto(this.Name, "txtCantidad", 20, 190);
            textBoxCantidad.Enabled = false;

            

            botonAceptar = Controles.GenerarBoton(this.Name, "btnAceptar", 10, 280, Button.BtnStyle.RectHalf, "Aceptar");

            botonAceptar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(botonAceptar_Click);
        }

        /// <summary>
        /// Método que se ejecuta cuando se reciben datos del puerto USB
        /// </summary>
        /// <param name="datos">Los datos recibidos</param>
        void MotorUSB_DatosRecibidos(string datos)
        {
            botonAceptar.Click -= new Intermec.Printer.UI.Canvas.MouseEventHandler(botonAceptar_Click);
            botonAceptar.Click += new Intermec.Printer.UI.Canvas.MouseEventHandler(botonAceptar_Click);
            if (datos.StartsWith("P"))
            {
                //if (datos.Length < 10) return; 
                textBoxNumeroParte.Text = datos.Replace("P",string.Empty);
                if (datos.Substring(1,datos.Length -1) != numeroDeParteEsperado)
                {
                    etiquetaResultadoVerificacion.Text = "El número de parte no corresponde";
                    etiquetaNumeroParte.ForeColor = Globales.ColorAplicacionError();
                    verificacionAprobada = false;
                    numeroDeParteAprobado = false;
                    
                }
                else
                {
                    etiquetaResultadoVerificacion.Text = string.Empty;
                    etiquetaNumeroParte.ForeColor = ICPUIClr.clrBlack;
                    numeroDeParteAprobado = true;
                }
            }else if(datos.StartsWith("Q"))
            {
                try
                {
                    int qty = int.Parse(datos.Substring(1, datos.Length - 1));
                }catch{
                    cantidadAprobada = false;
                }
                textBoxCantidad.Text = datos.Replace("Q", string.Empty);
                if (datos.Substring(1, datos.Length - 1) != cantidadEsperada.ToString())
                {
                    etiquetaResultadoVerificacion.Text = "La cantidad no corresponde";
                    etiquetaCantidad.ForeColor = Globales.ColorAplicacionError();
                    verificacionAprobada = false;
                    cantidadAprobada = false;
                }
                else
                {
                    etiquetaResultadoVerificacion.Text = string.Empty;
                    etiquetaCantidad.ForeColor = ICPUIClr.clrBlack;
                    cantidadAprobada = true;
                }
            }else
            {
                //etiquetaResultadoVerificacion.Text = "Los códigos escaneados son incorrectos";
                verificacionAprobada = false;
            }
            verificacionAprobada = (numeroDeParteAprobado && cantidadAprobada);
            //if (verificacionAprobada)
            //{
            //    //MotorUSB.DetenerLectura();
            //    Close();
            //    Aprobado();
            //}
            //else
            //{
            //    etiquetaResultadoVerificacion.Text = "Revise los datos antes de continuar";
            //}
        }

        /// <summary>
        /// Muestra la forma
        /// </summary>
        /// <param name="numeroParte">El número de parte que se espera para que la verificación sea aprobada</param>
        /// <param name="cantidad">La cantidad que se espera para que la verificación sea aprobada</param>
        public void Verificar(string numeroParte, int cantidad)
        {
            MotorUSB.IniciarEscaneo();
            MotorUSB.DatosRecibidos += new MotorUSB.MotorUSB_DatosRecibidos(MotorUSB_DatosRecibidos);
            numeroDeParteEsperado = numeroParte;
            cantidadEsperada = cantidad;
            base.Show();
        }

        /// <summary>
        /// Se presiona el botón aceptar
        /// </summary>
        private void botonAceptar_Click(object o, Intermec.Printer.UI.Canvas.MouseEventArgs eventArgs)
        {
            if (verificacionAprobada)
            {
                //MotorUSB.DetenerLectura();
                MotorUSB.DatosRecibidos -= new MotorUSB.MotorUSB_DatosRecibidos(MotorUSB_DatosRecibidos);
                Close();
                Aprobado();
            }
            else
            {
                etiquetaResultadoVerificacion.Text = "Datos incorrectos";
            }
        }



        #region IDisposable Members

        public void Dispose()
        {
            this.Close();
        }

        #endregion
    }
}
