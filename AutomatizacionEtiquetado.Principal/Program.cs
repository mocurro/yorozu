﻿using System;
using System.Collections.Generic;
using System.Text;
using AutomatizacionEtiquetado.UI.Aplicacion;
using AutomatizacionEtiquetado.Comun.Globales;
using Intermec.ICPUIElements;
using System.Threading;
using System.Globalization;
using Automatizacion_de_etiquetado.Enumeradores;
using AutomatizacionEtiquetado.Datos.Fabricas;
using AutomatizacionEtiquetado.Comun.Constantes;


namespace AutomatizacionEtiquetado.Principal
{
    class Program
    {

        public const int ConComparacion = 1;
        public const int SinComparacion = 2;
        public const int ArchivoVacio = 3;
        public const int FormatoInvalido = 4;
        
        static void Main(string[] args)
        {
            try
            {
                

                //FabricaNumeroParte fabrica = new FabricaNumeroParte();
                //String lectura = fabrica.AplicaionSeleccionada();
                //Program p = new Program();
                int result; //=  p.regresarAplicacionDeInicio(lectura);
                result = 2;
                switch(result){
                    case ConComparacion:
                     Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                        FormaPrincipal formaConComparacion = new FormaPrincipal("Principal", Globales.ColorAplicacion());
                        formaConComparacion.MostrarPantalla();               
                        Forms.Run();
                        break;

                    case SinComparacion:
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    FormaSinComparacion formaSinConfirmacion = new FormaSinComparacion("Principal", Globales.ColorAplicacion());
                    formaSinConfirmacion.MostrarPantalla();
                    Forms.Run();
                        break;
                    case ArchivoVacio:
                     Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    FormaErrorInicial formaError = new FormaErrorInicial("Principal", Globales.ColorAplicacion(), "El archivo configuracion.txt está vacío");
                    formaError.MostrarPantalla();
                    Forms.Run();
                        break;

                    case FormatoInvalido:
                        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    FormaErrorInicial formaErrorformato = new FormaErrorInicial("Principal", Globales.ColorAplicacion(), "Formato invalido");
                    formaErrorformato.MostrarPantalla();
                    Forms.Run();
                        break;

                       


                
                }



                
                /*

                if (result.Equals(IniciaraPalicacion.ArchivoVacio)) {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    FormaErrorInicial formaError = new FormaErrorInicial("Principal", Globales.ColorAplicacion(), "El archivo configruracion.txt está vacío");
                    formaError.MostrarPantalla();
                    Forms.Run();
                }
                else if (result.Equals(IniciaraPalicacion.ConComparacion)) {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                FormaPrincipal formaPrincipal = new FormaPrincipal("Principal", Globales.ColorAplicacion());
                formaPrincipal.MostrarPantalla();               
                Forms.Run();
                }
                else if (result.Equals(IniciaraPalicacion.SinComparacion))
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    FormaSinComparacion formaPrincipal = new FormaSinComparacion("Principal", Globales.ColorAplicacion());
                    formaPrincipal.MostrarPantalla();
                    Forms.Run();
                }
                else if (result.Equals(IniciaraPalicacion.FormatoInvalido))
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    FormaErrorInicial formaError = new FormaErrorInicial("Principal", Globales.ColorAplicacion(), "Formato invalido de archivo ");
                    formaError.MostrarPantalla();
                    Forms.Run();
                }
                else {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                    FormaErrorInicial formaError = new FormaErrorInicial("Principal", Globales.ColorAplicacion(), "Error inesperado " + result);
                    formaError.MostrarPantalla();
                    Forms.Run();
                
                }

                */

                
            }
            catch (Exception ex)
            {
                Globales.RegistrarEvento(ex.Message, TiposMensajes.ErrorInesperado);
            }


        }
            public int regresarAplicacionDeInicio(string op){

                try{
                int response = int.Parse(op);
                    if(response.Equals(ConComparacion))
                        return ConComparacion;
                    else if(response.Equals(SinComparacion))
                        return SinComparacion;
                    else if(response.Equals(ArchivoVacio))
                        return ArchivoVacio;
                    else 
                        return FormatoInvalido;

                
                }catch(Exception ex){
                return FormatoInvalido;
                }
            
            
            }
            
        }




        


        


     

    }

